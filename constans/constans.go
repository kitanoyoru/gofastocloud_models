package constans

import "gitlab.com/fastogt/gofastogt/gofastogt"

// Services constants
const DEFAULT_SERVICE_ROOT_DIR_PATH string = "~/streamer"
const MIN_PRICE float64 = 0.0
const DEFAULT_PRICE float64 = MIN_PRICE
const MAX_PRICE float64 = 1000.0

// Streams constans
const DEFAULT_IARC int = 18
const DEFAULT_RESTART_ATTEMPTS int = 10
const DEFAULT_LOOP bool = false
const DEFAULT_HAVE_VIDEO bool = true
const DEFAULT_HAVE_AUDIO bool = true
const DEFAULT_PHOENIX bool = false

const MIN_AUTO_EXIT_TIME int = 1
const MIN_AUDIO_SELECT int = 0
const DEFAULT_RELAY_VIDEO bool = false
const DEFAULT_RELAY_AUDIO bool = false
const MAX_VIDEO_DURATION_MSEC gofastogt.DurationMsec = (60 * 60 * 1000) * 24 * 365

const DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME int = 12 * 3600
const DEFAULT_TIMESHIFT_CHUNK_DURATION int = 120
