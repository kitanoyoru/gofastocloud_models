package unittests

import (
	"encoding/json"
	"testing"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
)

func TestUnmarshalJSON(t *testing.T) {

	// type NodeSettings
	testVariable2 := gofastocloud_models.NodeSettings{}
	testJSON := []byte(`{"host":"127.65.43.21:8081", "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"}`)
	err := json.Unmarshal(testJSON, &testVariable2)
	AssertNoError(t, err)
	AssertEqual(t, testVariable2.Host, "127.65.43.21:8081")
	AssertEqual(t, testVariable2.Key, gofastocloud_base.LicenseKey("3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"))

	testCases2 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "invalid host",
			testArray: []byte(`{"host":{"host":"127.65.43.218081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"}`),
		},
		{
			name:      "empty key",
			testArray: []byte(`{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": ""}`),
		},
		{
			name:      "invalid key",
			testArray: []byte(`{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"}`),
		},
	}

	for _, obj := range testCases2 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable2))
		})
	}

	// type HostAndAlias
	testVariable4 := gofastocloud_models.HttpsConfig{}
	testJSON = []byte(`{"key":"key_path", "cert": "cert_path"}`)
	err = json.Unmarshal(testJSON, &testVariable4)
	AssertNoError(t, err)
	AssertEqual(t, testVariable4.Key, "key_path")
	AssertEqual(t, testVariable4.Cert, "cert_path")

	testCases4 := []struct {
		name      string
		testArray []byte
	}{
		{
			name:      "nil key",
			testArray: []byte(`{"key":"", "cert":"cert_path"}`),
		},
		{
			name:      "nil cert",
			testArray: []byte(`{"key":"", "cert": ""}`),
		},
		{
			name:      "nil data",
			testArray: []byte(`{}`),
		},
	}

	for _, obj := range testCases4 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable4))
		})
	}

	// type Settings
	testVariable3 := gofastocloud_models.GoBackendSettings{}
	testJSON = []byte(`{"host":"127.65.43.21:8081", "hls_host": "http://127.65.43.21:8081", 
"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":"127.65.43.21:8081", "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"ss","log_path":"C/qwd/32d", "vod_info":{"type":1, "tmdb":{"key":"02cc111111fe47aa0dfd8986a665ec76"}}, "cors":true, "auth":"qw:gg", "preferred_url_scheme":"http", "auth_content":0,
"max_upload_file_size":45453,"webrtc":{"stun":"qqqw", "turn":"qqqq"},"https":{"key":"key_path", "cert":"cert_path"}}`)
	err = json.Unmarshal(testJSON, &testVariable3)
	AssertNoError(t, err)
	AssertEqual(t, testVariable3.Host, "127.65.43.21:8081")
	AssertEqual(t, testVariable3.HlsHost, "http://127.65.43.21:8081")
	AssertEqual(t, testVariable3.VodsHost, "http://127.65.43.21:8082")
	AssertEqual(t, testVariable3.CodsHost, "http://127.65.43.21:8083")
	AssertEqual(t, testVariable3.Node.Host, "127.65.43.21:8081")
	AssertEqual(t, testVariable3.LogLevel, "ss")
	AssertEqual(t, testVariable3.LogPath, "C/qwd/32d")
	AssertEqual(t, testVariable3.Cors, true)
	AssertEqual(t, *testVariable3.Auth, "qw:gg")
	AssertEqual(t, testVariable3.MaxUploadFileSize, int64(45453))
	AssertEqual(t, *testVariable3.WebRTC, gofastocloud_models.WebRTCSettings{Stun: "qqqw", Turn: "qqqq"})
	AssertEqual(t, testVariable3.VodInfo.Tmdb.Key, "02cc111111fe47aa0dfd8986a665ec76")
	AssertEqual(t, *testVariable3.HttpsConfig, gofastocloud_models.HttpsConfig{Key: "key_path", Cert: "cert_path"})

	testCases3 := []struct {
		name      string
		testArray []byte
	}{
		{
			name: "empty host",
			testArray: []byte(`{"host":"", "hls_host": "http://127.65.43.21:8081", 
			"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"ss","log_path":"C/qwd/32d" , "cors":true, "auth":"qw:gg", 
"max_upload_file_size":45453,"webrtc":{"stun":"qqqw", "turn":"qqqq"}}`),
		},
		{
			name: "invalid host",
			testArray: []byte(`{"host":"127.6543.21:8081", "hls_host": "http://127.65.43.21:8081", 
			"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"ss","log_path":"C/qwd/32d" , "cors":true, "auth":"qw:gg", 
"max_upload_file_size":45453,"webrtc":{"stun":"qqqw", "turn":"qqqq"}}`),
		},
		{
			name: "empty log_level",
			testArray: []byte(`{"host":"127.65.43.21:8081", "hls_host": "http://127.65.43.21:8081", 
			"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"","log_path":"C/qwd/32d" , "cors":true, "auth":"qw:gg", 
"max_upload_file_size":45453,"webrtc":{"stun":"qqqw", "turn":"qqqq"}}`),
		},
		{
			name: "nil cors",
			testArray: []byte(`{"host":"127.65.43.21:8081", "hls_host": "http://127.65.43.21:8081", 
			"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"ss","log_path":"C/qwd/32d", "auth":"qw:gg", 
"max_upload_file_size":45453,"webrtc":{"stun":"qqqw", "turn":"qqqq"}}`),
		},
		{
			name: "nil max_upload_file_size",
			testArray: []byte(`{"host":"127.65.43.21:8081", "hls_host": "http://127.65.43.21:8081", 
			"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"ss","log_path":"C/qwd/32d" , "cors":true, "auth":"qw:gg", "webrtc":{"stun":"qqqw", "turn":"qqqq"}}`),
		},
		{
			name: "format username:password Auth",
			testArray: []byte(`{"host":"127.65.43.21:8081", "hls_host": "http://127.65.43.21:8081", 
			"vods_host": "http://127.65.43.21:8082", "cods_host":"http://127.65.43.21:8083",
"node":{"host":{"host":"127.65.43.21:8081", "alias": "192.0.2.1"}, "key": "3423423523546863476348834853745723752735723858345723523852835235283694368348682392838238673583577"},
"log_level":"ss","log_path":"C/qwd/32d" , "cors":true, "auth": "qwgg", 
"max_upload_file_size":45453,"webrtc":{"stun":"qqqw", "turn":"qqqq"}}`),
		},
	}

	for _, obj := range testCases3 {
		t.Run(obj.name, func(t *testing.T) {
			AssertError(t, json.Unmarshal(obj.testArray, &testVariable3))
		})
	}

}
