package unittests

import (
	"encoding/json"
	"testing"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
)

func TestProviderFrontUnmarshalJSON(t *testing.T) {
	testJSON := []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0
	  }`)
	var provider front.ProviderFront
	err := json.Unmarshal(testJSON, &provider)
	AssertNil(t, err)

	// test pass by nil
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertNil(t, err)

	// test pass by empty
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertNil(t, err)

	// test mail
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test first_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": ,
		"last_name": "last_name",
		"password": "efwef",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test last_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": ,
		"password": "",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test type
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "",
		"last_name": "last_name",
		"password": "",
		"created_date": 1651756917062,
		"type":,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test status
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "",
		"created_date": 1651756917062,
		"type":1,
		"status":,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test country
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": ,
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test language
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": ,
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

	// test credits
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	AssertError(t, err)

}

func TestSubscriberFrontUnmarshalJSON(t *testing.T) {
	testJSON := []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	var subsriber front.SubscriberFront
	err := json.Unmarshal(testJSON, &subsriber)
	AssertNil(t, err)

	// test pass by nil
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertNil(t, err)

	// test pass by empty
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"sid":"1651756917",
		"password":"",
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test sid by nil
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertNil(t, err)

	// test sid by empty
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"sid":"",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test mail
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test first_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": ,
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test last_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": ,
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test type
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test status
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test devices_count
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": ,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test max_devices_count
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": ,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test country
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": ,
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test language
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":,
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	// test servers
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers":
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	AssertError(t, err)

	testLiveJSON := []byte(`{"email":"topilski@mail.ru","first_name":"Alex","last_name":"Top","exp_date":1654497736943,"sid":"1651756917","status":1,"max_devices_count":10,"language":"en","country":"BY","servers":["627614bdf9e908ccf4e57179"],"devices_count":0,"password":"1023224"}`)
	err = json.Unmarshal(testLiveJSON, &subsriber)
	AssertNil(t, err)
}

func TestIsEqualPrice(t *testing.T) {
	Tests := []struct {
		PriceOne player.PricePack
		PriceTwo player.PricePack
		Result   bool
	}{
		{
			PriceOne: player.PricePack{},
			PriceTwo: player.PricePack{},
			Result:   true,
		}, {
			PriceOne: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH,
			},
			PriceTwo: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH},
			Result: true,
		}, {
			PriceOne: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH,
			},
			PriceTwo: player.PricePack{
				Price:    2.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH},
			Result: false,
		}, {
			PriceOne: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH,
			},
			PriceTwo: player.PricePack{
				Price:    1.0,
				Currency: player.BYN,
				Type:     player.SUB_MONTH},
			Result: false,
		}, {
			PriceOne: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH,
			},
			PriceTwo: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.LIFE_TIME},
			Result: false,
		}, {
			PriceOne: player.PricePack{
				Price:    1.0,
				Currency: player.USD,
				Type:     player.SUB_MONTH,
			},
			PriceTwo: player.PricePack{
				Price:    2.0,
				Currency: player.RUB,
				Type:     player.LIFE_TIME},
			Result: false,
		},
	}
	for _, test := range Tests {
		expect := test.PriceOne.IsEqual(&test.PriceTwo)
		AssertEqual(t, expect, test.Result)
	}
}
