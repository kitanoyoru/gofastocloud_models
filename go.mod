module gitlab.com/fastogt/gofastocloud_models

go 1.19

require (
	gitlab.com/fastogt/gofastocloud v1.11.19
	gitlab.com/fastogt/gofastocloud_base v1.10.9
	gitlab.com/fastogt/gofastogt v1.7.9
	go.mongodb.org/mongo-driver v1.10.4 // don't change version, we should support Mongo 3.6
)

require golang.org/x/sys v0.5.0 // indirect
