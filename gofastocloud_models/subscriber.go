package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SubscriberFields struct {
	Email     string                 `bson:"email"             json:"email"`
	FirstName string                 `bson:"first_name"        json:"first_name"`
	LastName  string                 `bson:"last_name"         json:"last_name"`
	SID       string                 `bson:"sid"               json:"sid"`
	Password  string                 `bson:"password"          json:"password"`
	ExpDate   time.Time              `bson:"exp_date"          json:"exp_date"`
	Status    front.StatusSubscriber `bson:"status"            json:"status"`
	Country   string                 `bson:"country"           json:"country"`
	Language  string                 `bson:"language"          json:"language"`
	Servers   []primitive.ObjectID   `bson:"servers"           json:"servers"`
	Phone     *string                `bson:"phone,omitempty"             json:"phone,omitempty"`
	Details   *string                `bson:"details,omitempty"           json:"details,omitempty"`

	MaxDeviceCount uint64 `bson:"max_devices_count" json:"max_devices_count"`

	// filled by server
	CID           string               `bson:"cid"               json:"cid"`
	Subscriptions []primitive.ObjectID `bson:"subscriptions"     json:"subscriptions"`
	Devices       []Device             `bson:"devices"           json:"devices"`
	Playlists     []UserPlaylist       `bson:"playlists"         json:"playlists"`
	CreatedDate   *time.Time           `bson:"created_date,omitempty" json:"created_date,omitempty"`

	//UserFavoriteContent
	Streams []UserStream `bson:"streams" json:"streams"`
	Vods    []UserStream `bson:"vods" json:"vods"`
	Serials []UserSerial `bson:"serials" json:"serials"`
}

type Subscriber struct {
	ID               primitive.ObjectID `bson:"_id" json:"id"`
	SubscriberFields `bson:",inline"`
}

type UserStream struct {
	IDStream         primitive.ObjectID     `bson:"id"`
	Favorite         bool                   `bson:"favorite"`
	Private          bool                   `bson:"private"`
	Locked           bool                   `bson:"locked"`
	Recent           gofastogt.UtcTimeMsec  `bson:"recent"`
	InterruptionTime gofastogt.DurationMsec `bson:"interruption_time"`
}

func MakeUserStream(id primitive.ObjectID) *UserStream {
	return &UserStream{
		IDStream:         id,
		Favorite:         false,
		Private:          false,
		Locked:           false,
		Recent:           0,
		InterruptionTime: 0,
	}
}

type UserSerial struct {
	IDSerial primitive.ObjectID    `bson:"id"`
	Favorite bool                  `bson:"favorite"`
	Private  bool                  `bson:"private"`
	Locked   bool                  `bson:"locked"`
	Recent   gofastogt.UtcTimeMsec `bson:"recent"`
}

func MakeUserSerial(id primitive.ObjectID) *UserSerial {
	return &UserSerial{
		IDSerial: id,
		Favorite: false,
		Private:  false,
		Locked:   false,
		Recent:   0,
	}
}

type UserPlaylist struct {
	IDPlaylist primitive.ObjectID `bson:"id"`
	Name       string             `bson:"name"`
	Url        string             `bson:"url"`
	Selected   bool               `bson:"select"`
}

func (playlist *UserPlaylist) ToFront() *front.UserPlaylistFront {
	hexed := playlist.IDPlaylist.Hex()
	return &front.UserPlaylistFront{
		IDPlaylist: &hexed,
		Name:       playlist.Name,
		Url:        playlist.Url,
		Selected:   playlist.Selected,
	}
}

func MakeUserPlaylistFromFront(p *front.UserPlaylistFront) (*UserPlaylist, error) {
	var id primitive.ObjectID
	var err error
	if p.IDPlaylist != nil {
		id, err = primitive.ObjectIDFromHex(*p.IDPlaylist)
		if err != nil {
			return nil, err
		}
	} else {
		id = primitive.NewObjectID()
	}
	return &UserPlaylist{
		IDPlaylist: id,
		Name:       p.Name,
		Url:        p.Url,
		Selected:   p.Selected,
	}, nil
}

func (sub *Subscriber) ToFront() *front.SubscriberFront {
	servers := []string{}
	for _, s := range sub.Servers {
		servers = append(servers, primitive.ObjectID.Hex(s))
	}
	var created gofastogt.UtcTimeMsec
	if sub.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*sub.CreatedDate)
	}
	hexed := sub.ID.Hex()
	return &front.SubscriberFront{
		ID:             &hexed,
		Email:          sub.Email,
		FirstName:      sub.FirstName,
		LastName:       sub.LastName,
		Password:       &sub.Password,
		CreatedDate:    &created,
		ExpDate:        gofastogt.Time2UtcTimeMsec(sub.ExpDate),
		Status:         sub.Status,
		MaxDeviceCount: sub.MaxDeviceCount,
		DeviceCount:    uint64(len(sub.Devices)),
		Country:        sub.Country,
		Language:       sub.Language,
		Servers:        servers,
		SID:            &sub.SID,
		Phone:          sub.Phone,
		Details:        sub.Details,
	}
}

func MakeSubscriberFromFront(s *front.SubscriberFront) (*SubscriberFields, error) {
	subServers := []primitive.ObjectID{}
	for _, s := range s.Servers {
		serverID, err := primitive.ObjectIDFromHex(s)
		if err != nil {
			return nil, err
		}
		subServers = append(subServers, serverID)
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	var sid, pass string
	if s.SID != nil {
		sid = *s.SID
	}
	if s.Password != nil {
		pass = *s.Password
	}

	subscriber := SubscriberFields{
		Email:          s.Email,
		FirstName:      s.FirstName,
		LastName:       s.LastName,
		SID:            sid,
		CreatedDate:    &created,
		ExpDate:        gofastogt.UtcTime2Time(s.ExpDate),
		MaxDeviceCount: s.MaxDeviceCount,
		Servers:        subServers,
		Country:        s.Country,
		Language:       s.Language,
		Status:         s.Status,
		Password:       pass,
		Phone:          s.Phone,
		Details:        s.Details,
		Streams:        []UserStream{},
		Vods:           []UserStream{},
		Serials:        []UserSerial{},
	}
	return &subscriber, nil
}

func (s *SubscriberFields) ContainsSubscription(sid primitive.ObjectID) bool {
	for _, sub := range s.Subscriptions {
		if sub == sid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) ContainsUserStream(sid primitive.ObjectID) bool {
	for _, uStream := range s.Streams {
		if uStream.IDStream == sid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) ContainsUserVods(sid primitive.ObjectID) bool {
	for _, uVod := range s.Vods {
		if uVod.IDStream == sid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) ContainsUserSerial(sid primitive.ObjectID) bool {
	for _, uSerial := range s.Serials {
		if uSerial.IDSerial == sid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) ContainsPackage(pid primitive.ObjectID) bool {
	for _, pack := range s.Servers {
		if pack == pid {
			return true
		}
	}
	return false
}

func (s *SubscriberFields) GetDevice(id primitive.ObjectID) *Device {
	for _, device := range s.Devices {
		if device.IDDevice == id {
			return &device
		}
	}
	return nil
}
