package front

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// Description,ViewCount cant be empty
type SeasonFront struct {
	ID          *string                `json:"id,omitempty"`
	Name        string                 `json:"name"`
	Background  string                 `json:"background_url"`
	Icon        string                 `json:"icon"`
	Groups      []string               `json:"groups"`
	Description string                 `json:"description"`
	Season      int                    `json:"season"`
	Episodes    []string               `json:"episodes"`
	ViewCount   int                    `json:"view_count"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

func (s *SeasonFront) UnmarshalJSON(data []byte) error {
	request := struct {
		ID          *string                `json:"id,omitempty"`
		Name        *string                `json:"name"`
		Background  *string                `json:"background_url"`
		Icon        *string                `json:"icon"`
		Groups      []string               `json:"groups"`
		Description *string                `json:"description"`
		Season      *int                   `json:"season"`
		Episodes    []string               `json:"episodes"`
		ViewCount   *int                   `json:"view_count"`
		CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("name can't be empty")
	}
	if request.Background == nil {
		return errors.New("background can't be empty")
	}
	if request.Icon == nil {
		return errors.New("icon can't be empty")
	}
	if request.Season == nil {
		return errors.New("season can't be empty")
	}
	if request.Description != nil {
		s.Description = *request.Description
	}
	s.ID = request.ID
	s.Name = *request.Name
	s.Background = *request.Background
	s.Icon = *request.Icon
	s.Groups = request.Groups
	s.Season = *request.Season
	s.ViewCount = *request.ViewCount
	s.Episodes = request.Episodes
	s.CreatedDate = request.CreatedDate
	return nil
}

func (s *SeasonFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *SeasonFront) ToPlayer() player.SeasonInfo {
	var id string
	if s.ID != nil {
		id = *s.ID
	} else {
		id = "" // #FIXME: never should happend
	}
	var createDate gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		createDate = *s.CreatedDate
	} else {
		createDate = 0
	}
	return player.SeasonInfo{
		ID:          id,
		Name:        s.Name,
		Background:  s.Background,
		Icon:        s.Icon,
		Groups:      s.Groups,
		Description: s.Description,
		Season:      s.Season,
		Episodes:    s.Episodes,
		ViewCount:   s.ViewCount,
		CreatedDate: createDate,
	}
}
