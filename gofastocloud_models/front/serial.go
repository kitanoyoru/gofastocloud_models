package front

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type SerialFront struct {
	ID          *string               `json:"id,omitempty"`
	Name        string                `json:"name"`
	Background  string                `json:"background_url"`
	Icon        string                `json:"icon"`
	IARC        int                   `json:"iarc"`
	Groups      []string              `json:"groups"`
	Description string                `json:"description"`
	PrimeDate   gofastogt.UtcTimeMsec `json:"prime_date"`
	Seasons     []string              `json:"seasons"`
	Price       float64               `json:"price"`
	Visible     bool                  `json:"visible"`
	ViewCount   int                   `json:"view_count"`
	// new fields
	UserScore  float64  `json:"user_score"`
	Country    string   `json:"country"`
	Genres     []string `json:"genres"`
	Directors  []string `json:"directors"`
	Cast       []string `json:"cast"`
	Production []string `json:"production"`

	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

func (s *SerialFront) UnmarshalJSON(data []byte) error {
	request := struct {
		ID          *string                `json:"id,omitempty"`
		Name        *string                `json:"name"`
		Background  *string                `json:"background_url"`
		Icon        *string                `json:"icon"`
		IARC        *int                   `json:"iarc"`
		Groups      []string               `json:"groups"`
		Description *string                `json:"description"`
		PrimeDate   *gofastogt.UtcTimeMsec `json:"prime_date"`
		Seasons     []string               `json:"seasons"`
		Price       *float64               `json:"price"`
		Visible     *bool                  `json:"visible"`
		ViewCount   *int                   `json:"view_count"`
		UserScore   *float64               `json:"user_score"`
		Country     *string                `json:"country"`
		Genres      []string               `json:"genres"`
		Directors   []string               `json:"directors"`
		Cast        []string               `json:"cast"`
		Production  []string               `json:"production"`

		CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("name can't be empty")
	}
	if request.Background == nil {
		return errors.New("background can't be empty")
	}
	if request.Icon == nil {
		return errors.New("icon can't be empty")
	}
	if request.Price == nil {
		return errors.New("price can't be empty")
	}
	if request.Visible == nil {
		return errors.New("visible can't be empty")
	}
	if request.ViewCount == nil {
		return errors.New("view count can't be empty")
	}
	if request.PrimeDate == nil {
		return errors.New("prime date can't be empty")
	}
	if request.IARC == nil {
		return errors.New("IARC can't be empty")
	}
	if request.Description != nil {
		s.Description = *request.Description
	}
	s.ID = request.ID
	s.Name = *request.Name
	s.Background = *request.Background
	s.Icon = *request.Icon
	s.IARC = *request.IARC
	s.ViewCount = *request.ViewCount
	s.Groups = request.Groups
	s.Seasons = request.Seasons
	s.CreatedDate = request.CreatedDate
	s.Visible = *request.Visible
	s.Price = *request.Price
	s.PrimeDate = *request.PrimeDate
	s.UserScore = *request.UserScore
	s.Country = *request.Country
	s.Genres = request.Genres
	s.Directors = request.Directors
	s.Cast = request.Cast
	s.Production = request.Production
	return nil
}

func (s *SerialFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *SerialFront) ToPlayer() player.SerialInfo {
	var id string
	if s.ID != nil {
		id = *s.ID
	} else {
		id = ""
	}
	var createDate gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		createDate = *s.CreatedDate
	} else {
		createDate = 0
	}
	return player.SerialInfo{
		ID:          id,
		Name:        s.Name,
		Background:  s.Background,
		Icon:        s.Icon,
		IARC:        s.IARC,
		Groups:      s.Groups,
		Description: s.Description,
		PrimeDate:   s.PrimeDate,
		Seasons:     s.Seasons,
		ViewCount:   s.ViewCount,
		Price:       s.Price,
		CreatedDate: createDate,
		UserScore:   s.UserScore,
		Country:     s.Country,
		Genres:      s.Genres,
		Directors:   s.Directors,
		Cast:        s.Cast,
		Production:  s.Production,
	}
}
