package player

import (
	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

//Life Stream models for subscriber devices

type MetaURL struct {
	Name string `bson:"name" json:"name"`
	URL  string `bson:"url" json:"url"`
}

type StreamBaseInfo struct {
	IDStream    string                 `json:"id"`
	Groups      []string               `json:"groups"`
	IARC        int                    `json:"iarc"`
	Parts       []string               `json:"parts"`
	ViewCount   int                    `json:"view_count"`
	Meta        []MetaURL              `json:"meta"`
	Price       float64                `json:"price"`
	Substitles  []StreamSubtitles      `json:"subtitles"`
	PackageID   *string                `json:"pid,omitempty"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Video       bool                   `json:"video"`
	Audio       bool                   `json:"audio"`

	// user
	Favorite         bool                   `json:"favorite"`
	Locked           bool                   `json:"locked"`
	Recent           gofastogt.UtcTimeMsec  `json:"recent"`
	InterruptionTime gofastogt.DurationMsec `json:"interrupt_time"`
}

type StreamSubtitles struct {
	Url      string `json:"url"`
	Language string `json:"language"`
}

type EPG struct {
	IDFiled      string              `json:"id"`
	URLS         []string            `json:"urls"`
	DisplayName  string              `json:"display_name"`
	ICON         string              `json:"icon"`
	ProgramField []media.ProgramInfo `json:"programs"`
}

type ChannelInfo struct {
	StreamBaseInfo
	EpgInfo EPG  `json:"epg"`
	Archive bool `json:"archive"`
}

// VODS Stream models for subscriber devices
type MovieInfo struct {
	URLS          []string               `json:"urls"`
	Description   string                 `json:"description"`
	DisplayName   string                 `json:"display_name"`
	BackgroundURL string                 `json:"background_url"`
	PreviewIcon   string                 `json:"preview_icon"`
	TrailerUrl    string                 `json:"trailer_url"`
	UserScore     float64                `json:"user_score"`
	PrimeDate     gofastogt.UtcTimeMsec  `json:"prime_date"`
	Country       string                 `json:"country"`
	Genres        []string               `json:"genres"`
	Directors     []string               `json:"directors"`
	Cast          []string               `json:"cast"`
	Production    []string               `json:"production"`
	Substitles    []StreamSubtitles      `json:"subtitles"`
	Duration      gofastogt.DurationMsec `json:"duration"`
	VODType       media.VodType          `json:"type"`
}

type VodInfo struct {
	StreamBaseInfo
	VOD MovieInfo `json:"vod"`
}

// Catchup Stream models for subscriber devices
type CatchupInfo struct {
	ChannelInfo
	StartRecord gofastogt.UtcTimeMsec `json:"start"`
	StopRecord  gofastogt.UtcTimeMsec `json:"stop"`
}

type SeasonInfo struct {
	ID          string                `json:"id"`
	Name        string                `json:"name"`
	Background  string                `json:"background_url"`
	Icon        string                `json:"icon"`
	Groups      []string              `json:"groups"`
	Description string                `json:"description"`
	Season      int                   `json:"season"`
	Episodes    []string              `json:"episodes"`
	ViewCount   int                   `json:"view_count"`
	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`
	PackageID   *string               `json:"pid,omitempty"`
}

type SerialInfo struct {
	ID          string                `json:"id"`
	Name        string                `json:"name"`
	Background  string                `json:"background_url"`
	Icon        string                `json:"icon"`
	IARC        int                   `json:"iarc"`
	Groups      []string              `json:"groups"`
	Description string                `json:"description"`
	PrimeDate   gofastogt.UtcTimeMsec `json:"prime_date"`
	Seasons     []string              `json:"seasons"`
	Price       float64               `json:"price"`
	ViewCount   int                   `json:"view_count"`
	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`
	PackageID   *string               `json:"pid,omitempty"`
	// new fields
	UserScore  float64  `json:"user_score"`
	Country    string   `json:"country"`
	Genres     []string `json:"genres"`
	Directors  []string `json:"directors"`
	Cast       []string `json:"cast"`
	Production []string `json:"production"`

	// user
	Favorite bool                  `json:"favorite"`
	Locked   bool                  `json:"locked"`
	Recent   gofastogt.UtcTimeMsec `json:"recent"`
}
