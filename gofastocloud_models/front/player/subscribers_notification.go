package player

import (
	"encoding/json"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type NotificationType int

const (
	TEXT NotificationType = iota
	HYPERLINK
)

type NotificationTextInfo struct {
	Message          string                 `json:"message"`
	NotificationType NotificationType       `json:"type"`
	ShowTime         gofastogt.DurationMsec `json:"show_time"`
}

const (
	MIN_TIME_MSEC int64 = 1 * 1000
	MAX_TIME_MSEC int64 = 3600 * 1000
)

func (message *NotificationTextInfo) ToByte() (json.RawMessage, error) {
	return json.Marshal(message)
}

func (message *NotificationTextInfo) IsValid() bool {
	if message.NotificationType != TEXT && message.NotificationType != HYPERLINK {
		return false
	}
	if len(message.Message) == 0 || message.ShowTime <= gofastogt.DurationMsec(MIN_TIME_MSEC) || message.ShowTime >= gofastogt.DurationMsec(MAX_TIME_MSEC) {
		return false
	}
	return true
}
