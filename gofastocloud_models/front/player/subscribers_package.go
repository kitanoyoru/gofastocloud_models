package player

import "strings"

type PriceType int

const (
	SUB_DAY PriceType = iota
	SUB_MONTH
	SUB_YEAR
	LIFE_TIME
)

type Currency string

// Three-letter [ISO currency code](https://www.iso.org/iso-4217-currency-codes.html)
const (
	USD Currency = "USD"
	EUR Currency = "EUR"
	RUB Currency = "RUB"
	BYN Currency = "BYN"
)

func (c Currency) ToLower() string {
	return strings.ToLower(string(c))
}

func (c Currency) ToUpper() string {
	return strings.ToUpper(string(c))
}

type PricePack struct {
	Price    float64   `bson:"amount"   json:"amount"`
	Type     PriceType `bson:"type"     json:"type"`
	Currency Currency  `bson:"currency" json:"currency"`
}

func (p *PricePack) IsEqual(compare *PricePack) bool {
	return p.Type == compare.Type &&
		p.Price == compare.Price &&
		p.Currency == compare.Currency
}

type Package struct {
	ID            string        `json:"id"`
	Name          string        `json:"name"`
	Streams       []ChannelInfo `json:"streams"`
	Vods          []VodInfo     `json:"vods"`
	Episodes      []VodInfo     `json:"episodes"`
	Seasons       []SeasonInfo  `json:"seasons"`
	Serials       []SerialInfo  `json:"serials"`
	Catchups      []CatchupInfo `json:"catchups"`
	Price         *PricePack    `json:"price,omitempty"`
	Description   string        `json:"description"`
	BackgroundURL string        `json:"background_url"`
	Available     bool          `json:"available"`
}

func (pack *Package) IsPaid() bool {
	return pack.Price != nil
}
