package front

import (
	"encoding/json"
	"errors"
	"net/mail"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type StatusSubscriber int

const (
	NOT_ACTIVE_SUB StatusSubscriber = iota
	ACTIVE_SUB
	DELETED_SUB
)

type DeviceStatus int

const (
	NOT_ACTIVE_DEVICE DeviceStatus = iota
	ACTIVE_DEVICE
	BANNED_DEVICE
)

type DeviceOS struct {
	Name     gofastogt.PlatformType `bson:"name" json:"name"`
	Version  string                 `bson:"version" json:"version"`
	Arch     string                 `bson:"arch" json:"arch"`
	RamTotal int64                  `bson:"ram_total" json:"ram_total"`
	RamFree  int64                  `bson:"ram_free" json:"ram_free"`
}

func (os *DeviceOS) UnmarshalJSON(data []byte) error {
	request := struct {
		Name     *string `json:"name"`
		Version  *string `json:"version"`
		Arch     *string `json:"arch"`
		RamTotal *int64  `json:"ram_total"`
		RamFree  *int64  `json:"ram_free"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("os Name can't be empty")
	}

	if len(*request.Name) == 0 {
		return errors.New("invalid os name")
	}

	if request.Version == nil {
		return errors.New("os Version can't be empty")
	}
	if request.Arch == nil {
		return errors.New("os Arch can't be empty")
	}
	if request.RamTotal == nil {
		return errors.New("os RamTotal can't be empty")
	}
	if request.RamFree == nil {
		return errors.New("os RamFree can't be empty")
	}

	os.Name = gofastogt.MakePlatformTypeFromString(*request.Name)
	os.Version = *request.Version
	os.Arch = *request.Arch
	os.RamFree = *request.RamFree
	os.RamTotal = *request.RamTotal
	return nil
}

type CPUBrand string

type DeviceProject struct {
	Name    string `bson:"name" json:"name"`
	Version string `bson:"version" json:"version"`
}

func (d *DeviceProject) UnmarshalJSON(data []byte) error {
	request := struct {
		Name    *string `json:"name"`
		Version *string `json:"version"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("device project name can't be empty")
	}
	if request.Version == nil {
		return errors.New("device project version can't be empty")
	}
	d.Name = *request.Name
	d.Version = *request.Version
	return nil
}

type DevicesFront struct {
	IDDevice    *string                `json:"id,omitempty"`
	Name        string                 `json:"name"`
	Status      DeviceStatus           `json:"status"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

type DeviceLoginFront struct {
	DevicesFront
	OS             *DeviceOS               `json:"os,omitempty"`
	Project        *DeviceProject          `json:"project,omitempty"`
	CPU            *CPUBrand               `json:"cpu_brand,omitempty"`
	Logins         []DeviceLoginInfoFront  `json:"logins,omitempty"`
	ActiveDuration *gofastogt.DurationMsec `json:"active_duration,omitempty"`
	LastActiveTime *gofastogt.UtcTimeMsec  `json:"last_active,omitempty"`
}

type DeviceLoginInfoFront struct {
	IP        string                `json:"ip"`
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
	Location  string                `json:"loc,omitempty"`
	City      string                `json:"city,omitempty"`
	Country   string                `json:"country,omitempty"`
}

type UserPlaylistFront struct {
	IDPlaylist *string `json:"id,omitempty"`
	Name       string  `json:"name"`
	Url        string  `json:"url"`
	Selected   bool    `json:"select"`
}

type SubscriberFront struct {
	ID             *string                `json:"id,omitempty"`
	Email          string                 `json:"email"`
	FirstName      string                 `json:"first_name"`
	LastName       string                 `json:"last_name"`
	Password       *string                `json:"password,omitempty"`
	SID            *string                `json:"sid,omitempty"`
	CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	ExpDate        gofastogt.UtcTimeMsec  `json:"exp_date"`
	Status         StatusSubscriber       `json:"status"`
	DeviceCount    uint64                 `json:"devices_count"`
	MaxDeviceCount uint64                 `json:"max_devices_count"`
	Country        string                 `json:"country"`
	Language       string                 `json:"language"`
	Servers        []string               `json:"servers"`
	Phone          *string                `json:"phone,omitempty"`
	Details        *string                `json:"details,omitempty"`
}

func (s *SubscriberFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Id             *string                `json:"id,omitempty"`
		Email          *string                `json:"email"`
		FirstName      *string                `json:"first_name"`
		LastName       *string                `json:"last_name"`
		Password       *string                `json:"password,omitempty"`
		SID            *string                `json:"sid,omitempty"`
		Phone          *string                `json:"phone,omitempty"`
		Details        *string                `json:"details,omitempty"`
		CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
		ExpDate        *gofastogt.UtcTimeMsec `json:"exp_date"`
		Status         *StatusSubscriber      `json:"status"`
		DeviceCount    *uint64                `json:"devices_count"`
		MaxDeviceCount *uint64                `json:"max_devices_count"`
		Country        *string                `json:"country"`
		Language       *string                `json:"language"`
		Servers        *[]string              `json:"servers"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password != nil {
		if len(*request.Password) == 0 {
			return errors.New("password can't be empty")
		}
	}
	if request.SID != nil {
		if len(*request.SID) == 0 {
			return errors.New("sid can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.ExpDate == nil {
		return errors.New("exp_date can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.DeviceCount == nil {
		return errors.New("deviceCount can't be empty")
	}
	if request.MaxDeviceCount == nil {
		return errors.New("maxDeviceCount can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.Servers == nil {
		return errors.New("servers can't be empty")
	}

	s.ID = request.Id
	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = request.Password
	s.CreatedDate = request.CreatedDate
	s.ExpDate = *request.ExpDate
	s.Status = *request.Status
	s.DeviceCount = *request.DeviceCount
	s.MaxDeviceCount = *request.MaxDeviceCount
	s.Country = *request.Country
	s.Language = *request.Language
	s.Servers = *request.Servers
	s.SID = request.SID
	s.Phone = request.Phone
	s.Details = request.Details
	return nil
}

type SubscriberSignUpFront struct {
	Email     string  `json:"email"`
	FirstName string  `json:"first_name"`
	LastName  string  `json:"last_name"`
	Password  string  `json:"password"`
	Country   string  `json:"country"`
	Language  string  `json:"language"`
	Phone     *string `json:"phone,omitempty"`
}

func (s *SubscriberSignUpFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Email     *string `json:"email"`
		FirstName *string `json:"first_name"`
		LastName  *string `json:"last_name"`
		Password  *string `json:"password"`
		Country   *string `json:"country"`
		Language  *string `json:"language"`
		Phone     *string `json:"phone"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password can't be empty")
	}
	if request.Password != nil {
		if len(*request.Password) == 0 {
			return errors.New("password can't be empty")
		}
	}
	if request.Phone != nil {
		if len(*request.Phone) == 0 {
			return errors.New("phone can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = *request.Password
	s.Country = *request.Country
	s.Language = *request.Language
	s.Phone = request.Phone

	return nil
}

func MakeSubscriberFromFront(s SubscriberSignUpFront) *SubscriberFront {
	create := gofastogt.MakeUTCTimestamp()
	exp_date := gofastogt.UtcTime2Time(create).AddDate(100, 0, 0) //Default Exp date + 100 years
	return &SubscriberFront{
		Email:          s.Email,
		FirstName:      s.FirstName,
		LastName:       s.LastName,
		Password:       &s.Password,
		Country:        s.Country,
		Language:       s.Language,
		CreatedDate:    &create,
		ExpDate:        gofastogt.Time2UtcTimeMsec(exp_date),
		Status:         NOT_ACTIVE_SUB,
		DeviceCount:    0,  //Default count device
		MaxDeviceCount: 10, //Default count device
		Servers:        []string{},
	}
}
