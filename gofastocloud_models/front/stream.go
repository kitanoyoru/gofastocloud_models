package front

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	con "gitlab.com/fastogt/gofastocloud_models/constans"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type IBytesConverter interface {
	ToBytes() (json.RawMessage, error)
}

type IStreamFront struct {
	IBytesConverter

	ID          *string                  `json:"id,omitempty"`
	Name        string                   `json:"name"`
	Price       float64                  `json:"price"`
	Visible     bool                     `json:"visible"`
	IARC        int                      `json:"iarc"`
	ViewCount   int                      `json:"view_count"`
	Type        media.StreamType         `json:"type"`
	Output      []media.OutputUri        `json:"output"`
	Description string                   `json:"description"`
	TVGID       string                   `json:"tvg_id"`
	TVGName     string                   `json:"tvg_name"`
	TVGLogo     string                   `json:"tvg_logo"`
	Archive     bool                     `json:"archive"`
	Groups      []string                 `json:"groups"`
	Meta        []player.MetaURL         `json:"meta"`
	Parts       []string                 `json:"parts"`
	Subtitles   []player.StreamSubtitles `json:"subtitles"`
	Location    *gofastogt.Location      `json:"location,omitempty"`
	CreatedDate *gofastogt.UtcTimeMsec   `json:"created_date,omitempty"`
}

func (s *IStreamFront) IsVod() bool {
	return media.IsVodStreamType(s.Type)
}

func NewDefaultStream(typeStream media.StreamType) *IStreamFront {
	current := gofastogt.MakeUTCTimestamp()
	return &IStreamFront{
		CreatedDate: &current,
		Price:       con.DEFAULT_PRICE,
		Visible:     true,
		Archive:     false,
		IARC:        con.DEFAULT_IARC,
		ViewCount:   0,
		Type:        typeStream,
	}
}

type ProxyStreamFront struct {
	IStreamFront
}

func newProxyStream(typeStream media.StreamType) *ProxyStreamFront {
	return &ProxyStreamFront{IStreamFront: *NewDefaultStream(typeStream)}
}

func NewProxyStream() *ProxyStreamFront {
	return newProxyStream(media.STREAM_TYPE_PROXY)
}

func (s *ProxyStreamFront) ToPlayer() player.ChannelInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	programm := []media.ProgramInfo{}
	var urls []string
	for _, outputUrl := range s.Output {
		urls = append(urls, outputUrl.Uri)
	}
	epg := player.EPG{
		IDFiled:      s.TVGID,
		DisplayName:  s.Name,
		ICON:         s.TVGLogo,
		URLS:         urls,
		ProgramField: programm,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, Archive: s.Archive, EpgInfo: epg}
	return channel
}

func (s *ProxyStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type HardwareStreamFront struct {
	IStreamFront
	Runtime          *media.StreamStatisticInfo `json:"runtime"`
	FeedbackDir      string                     `json:"feedback_directory"`
	LogLevel         media.StreamLogLevel       `json:"log_level"`
	Input            []media.InputUri           `json:"input"`
	HaveVideo        bool                       `json:"have_video"`
	HaveAudio        bool                       `json:"have_audio"`
	AudioTracksCount int                        `json:"audio_tracks_count"`
	Loop             bool                       `json:"loop"`
	SelectedInput    int                        `json:"selected_input"`
	RestartAttempts  int                        `json:"restart_attempts"`
	AutoStart        bool                       `json:"auto_start"`

	AudioSelect               *int                        `json:"audio_select,omitempty"`
	AutoExitTime              *media.StreamTTL            `json:"auto_exit_time,omitempty"`
	ExtraConfig               *media.ExtraConfig          `json:"extra_config,omitempty"`
	NotificationStreamContact []NotificationStreamContact `json:"notification,omitempty"`
}

func newHardwareStream(typeStream media.StreamType) *HardwareStreamFront {
	return &HardwareStreamFront{
		LogLevel:                  media.LOG_LEVEL_INFO,
		RestartAttempts:           con.DEFAULT_RESTART_ATTEMPTS,
		HaveVideo:                 con.DEFAULT_HAVE_VIDEO,
		HaveAudio:                 con.DEFAULT_HAVE_AUDIO,
		Loop:                      con.DEFAULT_LOOP,
		SelectedInput:             0,
		AutoStart:                 false,
		AudioTracksCount:          1,
		AutoExitTime:              nil,
		ExtraConfig:               nil,
		AudioSelect:               nil,
		NotificationStreamContact: nil,
		IStreamFront:              *NewDefaultStream(typeStream),
	}
}

type NotificationStreamContact struct {
	Email                  string                 `json:"email"`
	NotificationStreamType NotificationStreamType `json:"type"`
}

type NotificationStreamType int

const (
	STREAM_FINISHED NotificationStreamType = iota
	STREAM_HIGHT_CPU_LOAD
)

func (n NotificationStreamType) IsValid() bool {
	switch n {
	case STREAM_FINISHED, STREAM_HIGHT_CPU_LOAD:
		return true
	}
	return false
}

func (n *NotificationStreamContact) UnmarshalJSON(data []byte) error {
	request := struct {
		Email                  string                 `json:"email"`
		NotificationStreamType NotificationStreamType `json:"alarm"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if !request.NotificationStreamType.IsValid() {
		return errors.New("invalid input NotificationStreamType")
	}
	n.Email = request.Email
	n.NotificationStreamType = request.NotificationStreamType
	return nil
}

func (s *HardwareStreamFront) ToPlayer() player.ChannelInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	programm := []media.ProgramInfo{}
	var urls []string
	for _, outputUrl := range s.Output {
		urls = append(urls, outputUrl.Uri)
	}
	epg := player.EPG{
		IDFiled:      s.TVGID,
		DisplayName:  s.Name,
		ICON:         s.TVGLogo,
		URLS:         urls,
		ProgramField: programm,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, Archive: s.Archive, EpgInfo: epg}
	return channel
}

type RelayStreamFront struct {
	HardwareStreamFront
	VideoParser *media.VideoParser `json:"video_parser,omitempty"`
	AudioParser *media.AudioParser `json:"audio_parser,omitempty"`
}

func NewRelayStream() *RelayStreamFront {
	return newRelayStream(media.STREAM_TYPE_RELAY)
}

func newRelayStream(typeStream media.StreamType) *RelayStreamFront {
	vparser := media.H264_PARSE
	aparser := media.AAC_PARSE
	return &RelayStreamFront{
		HardwareStreamFront: *newHardwareStream(typeStream),
		VideoParser:         &vparser,
		AudioParser:         &aparser,
	}
}

func (s *RelayStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type EncodeStreamFront struct {
	HardwareStreamFront
	RelayAudio bool             `json:"relay_audio"`
	RelayVideo bool             `json:"relay_video"`
	VideoCodec media.VideoCodec `json:"video_codec"`
	AudioCodec media.AudioCodec `json:"audio_codec"`

	Resample           *bool                     `json:"resample,omitempty"`
	Deinterlace        *bool                     `json:"deinterlace,omitempty"`
	Volume             *media.Volume             `json:"volume,omitempty"`
	FrameRate          *gofastogt.Rational       `json:"frame_rate,omitempty"`
	AudioChannelsCount *int                      `json:"audio_channels_count,omitempty"`
	AudioStabilization *media.AudioStabilization `json:"audio_stabilization,omitempty"`
	Size               *gofastogt.Size           `json:"size,omitempty"`
	MachineLearning    *media.MachineLearning    `json:"machine_learning,omitempty"`
	VideoBitrate       *media.Bitrate            `json:"video_bitrate,omitempty"`
	AudioBitrate       *media.Bitrate            `json:"audio_bitrate,omitempty"`
	Logo               *media.Logo               `json:"logo,omitempty"`
	RsvgLogo           *media.RSVGLogo           `json:"rsvg_logo,omitempty"`
	AspectRatio        *gofastogt.Rational       `json:"aspect_ratio,omitempty"`
	BackgroundEffect   *media.BackgroundEffect   `json:"background_effect,omitempty"`
	TextOverlay        *media.TextOverlay        `json:"text_overlay,omitempty"`
	VideoFlip          *media.VideoFlip          `json:"video_flip,omitempty"`
	StreamOverlay      *media.StreamOverlay      `json:"stream_overlay,omitempty"`
}

func NewEncodeStream() *EncodeStreamFront {
	return newEncodeStream(media.STREAM_TYPE_ENCODE)
}

func newEncodeStream(typeStream media.StreamType) *EncodeStreamFront {
	return &EncodeStreamFront{HardwareStreamFront: *newHardwareStream(typeStream),
		RelayAudio: con.DEFAULT_RELAY_AUDIO,
		RelayVideo: con.DEFAULT_RELAY_VIDEO,
		VideoCodec: media.X264_ENC,
		AudioCodec: media.FAAC}
}

func (s *EncodeStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type TimeshiftRecorderStreamFront struct {
	RelayStreamFront
	TimeshiftChunkDuration int `json:"timeshift_chunk_duration"`
	TimeshiftChunkLifeTime int `json:"timeshift_chunk_life_time"`
}

func NewTimeshiftRecorderStream() *TimeshiftRecorderStreamFront {
	return &TimeshiftRecorderStreamFront{
		RelayStreamFront:       *newRelayStream(media.STREAM_TYPE_TIMESHIFT_RECORDER),
		TimeshiftChunkDuration: con.DEFAULT_TIMESHIFT_CHUNK_DURATION,
		TimeshiftChunkLifeTime: con.DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME,
	}
}

func (s *TimeshiftRecorderStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type CatchupStreamFront struct {
	TimeshiftRecorderStreamFront
	StartRecord gofastogt.UtcTimeMsec `json:"start"`
	StopRecord  gofastogt.UtcTimeMsec `json:"stop"`
}

func NewCatchupStream() *CatchupStreamFront {
	ts := TimeshiftRecorderStreamFront{
		RelayStreamFront:       *newRelayStream(media.STREAM_TYPE_CATCHUP),
		TimeshiftChunkDuration: con.DEFAULT_TIMESHIFT_CHUNK_DURATION,
		TimeshiftChunkLifeTime: con.DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME,
	}
	return &CatchupStreamFront{
		TimeshiftRecorderStreamFront: ts,
		StartRecord:                  0,
		StopRecord:                   0,
	}
}

func (s *CatchupStreamFront) ToPlayer() player.CatchupInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	programm := []media.ProgramInfo{}
	var urls []string
	for _, outputUrl := range s.Output {
		urls = append(urls, outputUrl.Uri)
	}
	epg := player.EPG{
		IDFiled:      s.TVGID,
		DisplayName:  s.Name,
		ICON:         s.TVGLogo,
		URLS:         urls,
		ProgramField: programm,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, Archive: s.Archive, EpgInfo: epg}
	catchup := player.CatchupInfo{ChannelInfo: channel, StartRecord: s.StartRecord, StopRecord: s.StopRecord}
	return catchup
}

func (s *CatchupStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type VodBasedStreamFront struct {
	VODType       media.VodType          `json:"vod_type"`
	TrailerURL    string                 `json:"trailer_url"`
	BackgroundURL string                 `json:"background_url"`
	UserScore     float64                `json:"user_score"`
	PrimeDate     gofastogt.UtcTimeMsec  `json:"prime_date"`
	Country       string                 `json:"country"`
	Genres        []string               `json:"genres"`
	Directors     []string               `json:"directors"`
	Cast          []string               `json:"cast"`
	Production    []string               `json:"production"`
	Duration      gofastogt.DurationMsec `json:"duration"`
}

func (s *VodBasedStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func NewVodBasedStream() *VodBasedStreamFront {
	return &VodBasedStreamFront{
		VODType:   media.VODS,
		PrimeDate: 0,
		Country:   "US",
		UserScore: 0.0,
		Duration:  0,
	}
}

type VodProxyStreamFront struct {
	ProxyStreamFront
	VodBasedStreamFront
}

func NewVodProxyStream() *VodProxyStreamFront {
	return &VodProxyStreamFront{
		ProxyStreamFront:    *newProxyStream(media.STREAM_TYPE_VOD_PROXY),
		VodBasedStreamFront: *NewVodBasedStream(),
	}
}

func (s *VodProxyStreamFront) ToPlayer() player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, true, true)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

func (s *VodProxyStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type VodRelayStreamFront struct {
	RelayStreamFront
	VodBasedStreamFront
}

func NewVodRelayStream() *VodRelayStreamFront {
	return &VodRelayStreamFront{
		RelayStreamFront:    *newRelayStream(media.STREAM_TYPE_VOD_RELAY),
		VodBasedStreamFront: *NewVodBasedStream(),
	}
}

func (s *VodRelayStreamFront) ToPlayer() player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, s.HaveAudio, s.HaveVideo)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

func (s *VodRelayStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *VodEncodeStreamFront) ToPlayer() player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, s.HaveAudio, s.HaveVideo)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

type VodEncodeStreamFront struct {
	EncodeStreamFront
	VodBasedStreamFront
}

func (s *VodEncodeStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func NewVodEncodeStream() *VodEncodeStreamFront {
	return &VodEncodeStreamFront{
		EncodeStreamFront:   *newEncodeStream(media.STREAM_TYPE_VOD_ENCODE),
		VodBasedStreamFront: *NewVodBasedStream(),
	}
}

func MakeStreamBaseInfo(istream *IStreamFront, audio bool, video bool) *player.StreamBaseInfo {
	if len(istream.Parts) == 0 {
		istream.Parts = []string{}
	}
	if len(istream.Subtitles) == 0 {
		istream.Subtitles = []player.StreamSubtitles{}
	}
	return &player.StreamBaseInfo{
		IDStream:    *istream.ID,
		Groups:      istream.Groups,
		IARC:        istream.IARC,
		Parts:       istream.Parts,
		ViewCount:   istream.ViewCount,
		Meta:        istream.Meta,
		CreatedDate: istream.CreatedDate,
		Price:       istream.Price,
		Substitles:  istream.Subtitles,
		Audio:       audio,
		Video:       video,

		//
		Favorite:         false,
		Locked:           false,
		Recent:           0,
		InterruptionTime: 0,
	}
}

func MakeMovieInfo(baseVOD *VodBasedStreamFront, istream *IStreamFront) *player.MovieInfo {
	var urls []string
	for _, outputUrl := range istream.Output {
		urls = append(urls, outputUrl.Uri)
	}
	if len(baseVOD.Directors) == 0 {
		baseVOD.Directors = []string{}
	}
	if len(baseVOD.Cast) == 0 {
		baseVOD.Cast = []string{}
	}

	if len(baseVOD.Production) == 0 {
		baseVOD.Production = []string{}
	}

	if len(baseVOD.Genres) == 0 {
		baseVOD.Genres = []string{}
	}

	if len(istream.Subtitles) == 0 {
		istream.Subtitles = []player.StreamSubtitles{}
	}
	return &player.MovieInfo{
		DisplayName:   istream.Name,
		Description:   istream.Description,
		PreviewIcon:   istream.TVGLogo,
		URLS:          urls,
		BackgroundURL: baseVOD.BackgroundURL,
		TrailerUrl:    baseVOD.TrailerURL,
		UserScore:     baseVOD.UserScore,
		PrimeDate:     baseVOD.PrimeDate,
		Country:       baseVOD.Country,
		Directors:     baseVOD.Directors,
		Genres:        baseVOD.Genres,
		Production:    baseVOD.Production,
		Cast:          baseVOD.Cast,
		Substitles:    istream.Subtitles,
		Duration:      baseVOD.Duration,
		VODType:       baseVOD.VODType,
	}
}

type CodEncodeStreamFront struct {
	EncodeStreamFront
}

func (s *CodEncodeStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func NewCodEncodeStream() *CodEncodeStreamFront {
	return &CodEncodeStreamFront{
		EncodeStreamFront: EncodeStreamFront{HardwareStreamFront: *newHardwareStream(media.STREAM_TYPE_COD_ENCODE),
			RelayAudio: con.DEFAULT_RELAY_AUDIO,
			RelayVideo: con.DEFAULT_RELAY_VIDEO,
			VideoCodec: media.X264_ENC,
			AudioCodec: media.FAAC},
	}
}

type CodRelayStreamFront struct {
	RelayStreamFront
}

func NewCodRelayStream() *CodRelayStreamFront {
	return &CodRelayStreamFront{
		RelayStreamFront: *newRelayStream(media.STREAM_TYPE_COD_RELAY),
	}
}

func (s *CodRelayStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type ChangerRelayStreamFront struct {
	RelayStreamFront
}

func (s *ChangerRelayStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

type ChangerEncodeStreamFront struct {
	EncodeStreamFront
}

func NewChangerRelayStream() *ChangerRelayStreamFront {
	return &ChangerRelayStreamFront{
		RelayStreamFront: *newRelayStream(media.STREAM_TYPE_CHANGER_RELAY),
	}
}

func NewChangerEncodeStream() *ChangerEncodeStreamFront {
	return &ChangerEncodeStreamFront{
		EncodeStreamFront: *newEncodeStream(media.STREAM_TYPE_CHANGER_ENCODE),
	}
}

func (s *ChangerEncodeStreamFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}
