package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type PackageFields struct {
	Name          string              `bson:"name"            json:"name"`
	Host          front.WSServerFront `bson:"host"            json:"host"`
	Local         bool                `bson:"local"           json:"local"`
	Price         *player.PricePack   `bson:"price,omitempty" json:"price,omitempty"`
	Visible       bool                `bson:"visible"         json:"visible"`
	Description   string              `bson:"description"     json:"description"`
	BackgroundURL string              `bson:"background_url"  json:"background_url"`
	// filled by server
	CreatedDate *time.Time `bson:"created_date,omitempty" json:"created_date,omitempty"`
	PID         *string    `bson:"pid,omitempty"                    json:"pid,omitempty"`
}

func (ser *PackageFields) IsPaid() bool {
	return ser.Price != nil
}

type Package struct {
	ID            primitive.ObjectID `bson:"_id" json:"id"`
	PackageFields `bson:",inline"`
}

func (s *Package) ToFront() *front.PackageFront {
	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}
	return &front.PackageFront{
		ID:            &hexed,
		Name:          s.Name,
		Host:          s.Host,
		Local:         s.Local,
		Price:         s.Price,
		Visible:       s.Visible,
		Description:   s.Description,
		BackgroundURL: s.BackgroundURL,
		CreatedDate:   &created,
	}
}

func MakePackageFromFront(s *front.PackageFront) *PackageFields {
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	} else {
		created = time.Now()
	}

	result := PackageFields{
		Name:          s.Name,
		Host:          s.Host,
		Local:         s.Local,
		Price:         s.Price,
		Description:   s.Description,
		BackgroundURL: s.BackgroundURL,
		Visible:       s.Visible,
		CreatedDate:   &created,
	}
	return &result
}
