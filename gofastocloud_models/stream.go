package gofastocloud_models

import (
	"encoding/json"
	"errors"
	"time"

	"gitlab.com/fastogt/gofastocloud/gofastocloud/media"
	con "gitlab.com/fastogt/gofastocloud_models/constans"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IStreamFields struct {
	front.IBytesConverter
	Name       string            `bson:"name" json:"name"`
	Price      float64           `bson:"price" json:"price"`
	Visible    bool              `bson:"visible" json:"visible"`
	IARC       int               `bson:"iarc" json:"iarc"`
	ViewCount  int               `bson:"view_count" json:"view_count"`
	Output     []media.OutputUri `bson:"output" json:"output"`
	Archive    bool              `bson:"archive" json:"archive"`
	TypeStream media.StreamType  `bson:"type_stream" json:"type_stream"`
	//optional fields
	Description string              `bson:"description,omitempty" json:"description,omitempty"`
	TVGID       string              `bson:"tvg_id,omitempty" json:"tvg_id,omitempty"`
	TVGName     string              `bson:"tvg_name,omitempty" json:"tvg_name,omitempty"`
	TVGLogo     string              `bson:"tvg_logo,omitempty" json:"tvg_logo,omitempty"`
	Groups      []string            `bson:"groups,omitempty" json:"groups,omitempty"`
	Parts       []string            `bson:"parts,omitempty" json:"parts,omitempty"`
	Meta        []player.MetaURL    `bson:"meta,omitempty" json:"meta,omitempty"`
	Location    *gofastogt.Location `bson:"location,omitempty" json:"location,omitempty"`

	// server setup
	Subtitles   []player.StreamSubtitles `bson:"subtitles,omitempty"         json:"subtitles,omitempty"`
	CreatedDate *time.Time               `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

type IStream struct {
	IStreamFields `bson:",inline"`
	ID            primitive.ObjectID `bson:"_id" json:"id"`
}

func NewDefaultStream(typeStream media.StreamType) *IStream {
	current := time.Now()
	return &IStream{
		IStreamFields: IStreamFields{
			CreatedDate: &current,
			Price:       con.DEFAULT_PRICE,
			Visible:     true,
			Archive:     false,
			IARC:        con.DEFAULT_IARC,
			ViewCount:   0,
			TypeStream:  typeStream,
		},
	}
}

func (s *IStream) ToFront() *front.IStreamFront {
	if len(s.Groups) == 0 {
		s.Groups = []string{}
	}
	if len(s.Output) == 0 {
		s.Output = []media.OutputUri{}
	}
	if len(s.Meta) == 0 {
		s.Meta = []player.MetaURL{}
	}
	if len(s.Subtitles) == 0 {
		s.Subtitles = []player.StreamSubtitles{}
	}
	if len(s.Parts) == 0 {
		s.Parts = []string{}
	}

	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}
	return &front.IStreamFront{
		ID:          &hexed,
		Name:        s.Name,
		Price:       s.Price,
		Groups:      s.Groups,
		Visible:     s.Visible,
		IARC:        s.IARC,
		ViewCount:   s.ViewCount,
		Type:        s.TypeStream,
		Output:      s.Output,
		Description: s.Description,
		TVGLogo:     s.TVGLogo,
		TVGID:       s.TVGID,
		TVGName:     s.TVGName,
		Archive:     s.Archive,
		Meta:        s.Meta,
		Parts:       s.Parts,
		Subtitles:   s.Subtitles,
		Location:    s.Location,
		CreatedDate: &created,
	}
}

func (s *IStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *IStream) GetConfig() *media.BaseConfig {
	conf := media.BaseConfig{Id: media.StreamId(s.ID.Hex()),
		Type:   s.TypeStream,
		Output: s.Output}
	return &conf
}

type ProxyStream struct {
	IStream `bson:",inline"`
}

func newProxyStream(typeStream media.StreamType) *ProxyStream {
	return &ProxyStream{IStream: *NewDefaultStream(typeStream)}
}

func NewProxyStream() *ProxyStream {
	return newProxyStream(media.STREAM_TYPE_PROXY)
}

func (p *ProxyStream) ToFront() *front.ProxyStreamFront {
	return &front.ProxyStreamFront{
		IStreamFront: *p.IStream.ToFront(),
	}
}

func (s *ProxyStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *ProxyStream) GetConfig() *media.ProxyConfig {
	config := media.ProxyConfig{
		BaseConfig: *s.IStream.GetConfig(),
	}
	return &config
}

type HardwareStream struct {
	IStream          `bson:",inline"`
	LogLevel         media.StreamLogLevel `bson:"log_level" json:"log_level"`
	Input            []media.InputUri     `bson:"input" json:"input"`
	HaveVideo        bool                 `bson:"have_video" json:"have_video"`
	HaveAudio        bool                 `bson:"have_audio" json:"have_audio"`
	AudioTracksCount int                  `bson:"audio_tracks_count" json:"audio_tracks_count"`
	Loop             bool                 `bson:"loop" json:"loop"`
	SelectedInput    int                  `bson:"selected_input" json:"selected_input"`
	RestartAttempts  int                  `bson:"restart_attempts" json:"restart_attempts"`
	AutoStart        bool                 `bson:"auto_start" json:"auto_start"`

	AudioSelect               *int                              `bson:"audio_select,omitempty" json:"audio_select,omitempty"`
	AutoExitTime              *media.StreamTTL                  `bson:"auto_exit_time,omitempty" json:"auto_exit_time,omitempty"`
	ExtraConfig               *media.ExtraConfig                `bson:"extra_config,omitempty" json:"extra_config,omitempty"`
	NotificationStreamContact []front.NotificationStreamContact `bson:"notification,omitempty" json:"notification,omitempty"`
}

func newHardwareStream(typeStream media.StreamType) *HardwareStream {
	return &HardwareStream{
		LogLevel:                  media.LOG_LEVEL_INFO,
		RestartAttempts:           con.DEFAULT_RESTART_ATTEMPTS,
		HaveVideo:                 con.DEFAULT_HAVE_VIDEO,
		HaveAudio:                 con.DEFAULT_HAVE_AUDIO,
		Loop:                      con.DEFAULT_LOOP,
		SelectedInput:             0,
		AutoStart:                 false,
		AudioTracksCount:          1,
		AutoExitTime:              nil,
		ExtraConfig:               nil,
		AudioSelect:               nil,
		NotificationStreamContact: nil,
		IStream:                   *NewDefaultStream(typeStream),
	}
}

func (s *HardwareStream) ToFront() *front.HardwareStreamFront {
	if len(s.Input) == 0 {
		s.Input = []media.InputUri{}
	}
	feedback := media.MakeFeedbackDir(media.StreamId(s.ID.Hex()))
	hardwareToFront := front.HardwareStreamFront{
		IStreamFront:              *s.IStream.ToFront(),
		FeedbackDir:               feedback,
		LogLevel:                  s.LogLevel,
		HaveVideo:                 s.HaveVideo,
		HaveAudio:                 s.HaveAudio,
		Loop:                      s.Loop,
		SelectedInput:             s.SelectedInput,
		Input:                     s.Input,
		AutoStart:                 s.AutoStart,
		AudioTracksCount:          s.AudioTracksCount,
		AutoExitTime:              s.AutoExitTime,
		ExtraConfig:               s.ExtraConfig,
		AudioSelect:               s.AudioSelect,
		RestartAttempts:           s.RestartAttempts,
		NotificationStreamContact: s.NotificationStreamContact,
	}
	// FIXME Runtime Stats.
	hardwareToFront.Runtime = &media.StreamStatisticInfo{}
	hardwareToFront.Runtime.Quality = hardwareToFront.Runtime.CalcQuality()
	if len(hardwareToFront.Runtime.OutputStreams) == 0 {
		hardwareToFront.Runtime.OutputStreams = []media.OutputStreamStatisticInfo{}
	}
	if len(hardwareToFront.Runtime.InputStreams) == 0 {
		hardwareToFront.Runtime.InputStreams = []media.InputStreamStatisticInfo{}
	}
	return &hardwareToFront
}

func (s *HardwareStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *HardwareStream) GetConfig() *media.HardwareConfig {
	feedbackDir := media.MakeFeedbackDir(media.StreamId(s.ID.Hex()))
	dataDir := media.MakeDataDir(media.StreamId(s.ID.Hex()))
	return &media.HardwareConfig{
		BaseConfig: *s.IStream.GetConfig(), FeedbackDir: feedbackDir, DataDir: dataDir, LogLevel: s.LogLevel, Loop: s.Loop, SelectedInput: s.SelectedInput,
		HaveVideo: s.HaveVideo, HaveAudio: s.HaveAudio, AutoExitTime: s.AutoExitTime, ExtraConfig: s.ExtraConfig, RestartAttempts: s.RestartAttempts,
		Input: s.Input, AudioSelect: s.AudioSelect, AudioTracksCount: s.AudioTracksCount,
	}
}

func (s *HardwareStream) StableForStreaming() {
	stabled := []media.OutputUri{}
	for _, out := range s.Output {
		conf := s.GetConfig()
		stabled = append(stabled, out.StableForStreaming(conf.Type, conf.Id))
	}
	s.Output = stabled
}

type RelayStream struct {
	HardwareStream `bson:",inline"`
	VideoParser    *media.VideoParser `bson:"video_parser,omitempty" json:"video_parser,omitempty"`
	AudioParser    *media.AudioParser `bson:"audio_parser,omitempty" json:"audio_parser,omitempty"`
}

func NewRelayStream() *RelayStream {
	return newRelayStream(media.STREAM_TYPE_RELAY)
}

func newRelayStream(typeStream media.StreamType) *RelayStream {
	vparser := media.H264_PARSE
	aparser := media.AAC_PARSE
	return &RelayStream{
		HardwareStream: *newHardwareStream(media.STREAM_TYPE_RELAY),
		VideoParser:    &vparser,
		AudioParser:    &aparser,
	}
}

func (s *RelayStream) ToFront() *front.RelayStreamFront {
	return &front.RelayStreamFront{
		HardwareStreamFront: *s.HardwareStream.ToFront(),
		VideoParser:         s.VideoParser,
		AudioParser:         s.AudioParser,
	}
}

func (s *RelayStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *RelayStream) GetConfig() *media.RelayConfig {
	conf := media.RelayConfig{HardwareConfig: *s.HardwareStream.GetConfig(), AudioParser: s.AudioParser, VideoParser: s.VideoParser}
	return &conf
}

type EncodeStream struct {
	HardwareStream `bson:",inline"`
	RelayAudio     bool             `bson:"relay_audio" json:"relay_audio"`
	RelayVideo     bool             `bson:"relay_video" json:"relay_video"`
	VideoCodec     media.VideoCodec `bson:"video_codec" json:"video_codec"`
	AudioCodec     media.AudioCodec `bson:"audio_codec" json:"audio_codec"`
	// audio optional
	Resample           *bool                     `bson:"resample,omitempty" json:"resample,omitempty"`
	AudioChannelsCount *int                      `bson:"audio_channels_count,omitempty" json:"audio_channels_count,omitempty"`
	Volume             *media.Volume             `bson:"volume,omitempty" json:"volume,omitempty"`
	AudioBitrate       *media.Bitrate            `bson:"audio_bitrate,omitempty" json:"audio_bitrate,omitempty"`
	AudioStabilization *media.AudioStabilization `bson:"audio_stabilization,omitempty" json:"audio_stabilization,omitempty"`
	// video optional
	Deinterlace      *bool                   `bson:"deinterlace,omitempty" json:"deinterlace,omitempty"`
	FrameRate        *gofastogt.Rational     `bson:"frame_rate,omitempty" json:"frame_rate,omitempty"`
	Size             *gofastogt.Size         `bson:"size,omitempty" json:"size,omitempty"`
	MachineLearning  *media.MachineLearning  `bson:"machine_learning,omitempty" json:"machine_learning,omitempty"`
	VideoBitrate     *media.Bitrate          `bson:"video_bitrate,omitempty" json:"video_bitrate,omitempty"`
	Logo             *media.Logo             `bson:"logo,omitempty" json:"logo,omitempty"`
	RsvgLogo         *media.RSVGLogo         `bson:"rsvg_logo,omitempty" json:"rsvg_logo,omitempty"`
	AspectRatio      *gofastogt.Rational     `bson:"aspect_ratio,omitempty" json:"aspect_ratio,omitempty"`
	BackgroundEffect *media.BackgroundEffect `bson:"background_effect,omitempty" json:"background_effect,omitempty"`
	TextOverlay      *media.TextOverlay      `bson:"text_overlay,omitempty" json:"text_overlay,omitempty"`
	VideoFlip        *media.VideoFlip        `bson:"video_flip,omitempty" json:"video_flip,omitempty"`
	StreamOverlay    *media.StreamOverlay    `bson:"stream_overlay,omitempty" json:"stream_overlay,omitempty"`
}

func NewEncodeStream() *EncodeStream {
	return newEncodeStream(media.STREAM_TYPE_ENCODE)
}

func newEncodeStream(typeStream media.StreamType) *EncodeStream {
	return &EncodeStream{HardwareStream: *newHardwareStream(media.STREAM_TYPE_ENCODE),
		RelayAudio: con.DEFAULT_RELAY_AUDIO,
		RelayVideo: con.DEFAULT_RELAY_VIDEO,
		VideoCodec: media.X264_ENC,
		AudioCodec: media.FAAC}
}

func (s *EncodeStream) ToFront() *front.EncodeStreamFront {
	return &front.EncodeStreamFront{
		HardwareStreamFront: *s.HardwareStream.ToFront(),
		RelayAudio:          s.RelayAudio,
		RelayVideo:          s.RelayVideo,
		VideoCodec:          s.VideoCodec,
		AudioCodec:          s.AudioCodec,
		Resample:            s.Resample,
		Deinterlace:         s.Deinterlace,
		Volume:              s.Volume,
		FrameRate:           s.FrameRate,
		AudioStabilization:  s.AudioStabilization,
		AudioChannelsCount:  s.AudioChannelsCount,
		Size:                s.Size,
		MachineLearning:     s.MachineLearning,
		VideoBitrate:        s.VideoBitrate,
		AudioBitrate:        s.AudioBitrate,
		Logo:                s.Logo,
		RsvgLogo:            s.RsvgLogo,
		AspectRatio:         s.AspectRatio,
		BackgroundEffect:    s.BackgroundEffect,
		TextOverlay:         s.TextOverlay,
		VideoFlip:           s.VideoFlip,
		StreamOverlay:       s.StreamOverlay,
	}
}

func (s *EncodeStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *EncodeStream) GetConfig() *media.EncodeConfig {
	conf := media.EncodeConfig{HardwareConfig: *s.HardwareStream.GetConfig(), RelayAudio: s.RelayAudio, RelayVideo: s.RelayVideo, Deinterlace: s.Deinterlace,
		Resample: s.Resample, Volume: s.Volume, VideoCodec: s.VideoCodec, AudioCodec: s.AudioCodec, FrameRate: s.FrameRate,
		AudioChannelsCount: s.AudioChannelsCount, Size: s.Size, MachineLearning: s.MachineLearning, VideoBitrate: s.VideoBitrate,
		AudioBitrate: s.AudioBitrate, AudioStabilization: s.AudioStabilization, Logo: s.Logo, RsvgLogo: s.RsvgLogo, AspectRatio: s.AspectRatio,
		BackgroundEffect: s.BackgroundEffect, TextOverlay: s.TextOverlay, VideoFlip: s.VideoFlip}
	return &conf
}

type TimeshiftRecorderStream struct {
	RelayStream            `bson:",inline"`
	TimeshiftChunkDuration int `bson:"timeshift_chunk_duration" json:"timeshift_chunk_duration"`
	TimeshiftChunkLifeTime int `bson:"timeshift_chunk_life_time" json:"timeshift_chunk_life_time"`
}

func NewTimeshiftRecorderStream() *TimeshiftRecorderStream {
	return &TimeshiftRecorderStream{
		RelayStream:            *newRelayStream(media.STREAM_TYPE_TIMESHIFT_RECORDER),
		TimeshiftChunkDuration: con.DEFAULT_TIMESHIFT_CHUNK_DURATION,
		TimeshiftChunkLifeTime: con.DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME,
	}
}

func (s *TimeshiftRecorderStream) ToFront() *front.TimeshiftRecorderStreamFront {
	return &front.TimeshiftRecorderStreamFront{
		RelayStreamFront:       *s.RelayStream.ToFront(),
		TimeshiftChunkDuration: s.TimeshiftChunkDuration,
		TimeshiftChunkLifeTime: s.TimeshiftChunkLifeTime,
	}
}

func (s *TimeshiftRecorderStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}
func (s *TimeshiftRecorderStream) GetConfig() *media.TimeShiftRecordConfig {
	return &media.TimeShiftRecordConfig{
		RelayConfig:            *s.RelayStream.GetConfig(),
		TimeshiftChunkDuration: s.TimeshiftChunkDuration,
		TimeshiftChunkLifeTime: s.TimeshiftChunkLifeTime,
	}
}

type CatchupStream struct {
	TimeshiftRecorderStream `bson:",inline"`
	StartRecord             time.Time `bson:"start" json:"start"`
	StopRecord              time.Time `bson:"stop" json:"stop"`
}

func NewCatchupStream() *CatchupStream {
	ts := TimeshiftRecorderStream{
		RelayStream:            *newRelayStream(media.STREAM_TYPE_CATCHUP),
		TimeshiftChunkDuration: con.DEFAULT_TIMESHIFT_CHUNK_DURATION,
		TimeshiftChunkLifeTime: con.DEFAULT_TIMESHIFT_CHUNK_LIFE_TIME,
	}
	return &CatchupStream{
		TimeshiftRecorderStream: ts,
		StartRecord:             time.Unix(0, 0),
		StopRecord:              time.Unix(0, 0),
	}
}

func (s *CatchupStream) ToFront() *front.CatchupStreamFront {
	return &front.CatchupStreamFront{
		TimeshiftRecorderStreamFront: *s.TimeshiftRecorderStream.ToFront(),
		StartRecord:                  gofastogt.Time2UtcTimeMsec(s.StartRecord),
		StopRecord:                   gofastogt.Time2UtcTimeMsec(s.StopRecord),
	}
}

func (s *CatchupStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *CatchupStream) GetConfig() *media.CatchupConfig {
	return &media.CatchupConfig{
		TimeShiftRecordConfig: *s.TimeshiftRecorderStream.GetConfig(),
		StartRecord:           int(gofastogt.Time2UtcTimeMsec(s.StartRecord)),
		StopRecord:            int(gofastogt.Time2UtcTimeMsec(s.StopRecord)),
	}
}

type TimeshiftPlayerStream struct {
	RelayStream    `bson:",inline"`
	TimeshiftDir   string `bson:"timeshift_dir" json:"timeshift_dir"`
	TimeshiftDelay int    `bson:"timeshift_delay" json:"timeshift_delay"`
}

func NewTimeshiftPlayerStream() *TimeshiftPlayerStream {
	return &TimeshiftPlayerStream{
		RelayStream: *newRelayStream(media.STREAM_TYPE_TIMESHIFT_PLAYER),
	}
}

func (s *TimeshiftPlayerStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *TimeshiftPlayerStream) GetConfig() *media.TimeshiftPlayerConfig {
	return &media.TimeshiftPlayerConfig{
		RelayConfig:    *s.RelayStream.GetConfig(),
		TimeshiftDir:   s.TimeshiftDir,
		TimeshiftDelay: s.TimeshiftDelay,
	}
}

type TestLifeStream struct {
	RelayStream `bson:",inline"`
}

func NewTestLifeStream() *TestLifeStream {
	return &TestLifeStream{
		RelayStream: *newRelayStream(media.STREAM_TYPE_TEST_LIFE),
	}
}

func (s *TestLifeStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *TestLifeStream) GetConfig() *media.TestLifeConfig {
	return &media.TestLifeConfig{
		RelayConfig: *s.RelayStream.GetConfig(),
	}
}

type CodRelayStream struct {
	RelayStream `bson:",inline"`
}

func NewCodRelayStream() *CodRelayStream {
	return &CodRelayStream{
		RelayStream: *newRelayStream(media.STREAM_TYPE_COD_RELAY),
	}
}

func (s *CodRelayStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *CodRelayStream) ToFront() *front.CodRelayStreamFront {
	return &front.CodRelayStreamFront{
		RelayStreamFront: *s.RelayStream.ToFront(),
	}
}

func (s *CodRelayStream) GetConfig() *media.CodRelayConfig {
	return &media.CodRelayConfig{
		RelayConfig: *s.RelayStream.GetConfig(),
	}
}

type CodEncodeStream struct {
	EncodeStream `bson:",inline"`
}

func NewCodEncodeStream() *CodEncodeStream {
	return &CodEncodeStream{
		EncodeStream: *newEncodeStream(media.STREAM_TYPE_COD_ENCODE),
	}
}

func (s *CodEncodeStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *CodEncodeStream) ToFront() *front.CodEncodeStreamFront {
	return &front.CodEncodeStreamFront{
		EncodeStreamFront: *s.EncodeStream.ToFront(),
	}
}

func (s *CodEncodeStream) GetConfig() *media.CodEncodeConfig {
	return &media.CodEncodeConfig{
		EncodeConfig: *s.EncodeStream.GetConfig(),
	}
}

type VodBasedStream struct {
	VODType       media.VodType          `bson:"vod_type" json:"vod_type"`
	TrailerURL    string                 `bson:"trailer_url" json:"trailer_url"`
	BackgroundURL string                 `bson:"background_url" json:"background_url"`
	UserScore     float64                `bson:"user_score" json:"user_score"`
	PrimeDate     time.Time              `bson:"prime_date" json:"prime_date"`
	Country       string                 `bson:"country" json:"country"`
	Genres        []string               `bson:"genres" json:"genres"`
	Directors     []string               `bson:"directors" json:"directors"`
	Cast          []string               `bson:"cast" json:"cast"`
	Production    []string               `bson:"production" json:"production"`
	Duration      gofastogt.DurationMsec `bson:"duration" json:"duration"`
}

func NewVodBasedStream() *VodBasedStream {
	return &VodBasedStream{
		VODType:   media.VODS,
		PrimeDate: time.Unix(0, 0),
		Country:   "US",
		UserScore: 0.0,
		Duration:  0,
	}
}

func (s *VodBasedStream) ToFront() *front.VodBasedStreamFront {
	if len(s.Directors) == 0 {
		s.Directors = []string{}
	}
	if len(s.Cast) == 0 {
		s.Cast = []string{}
	}

	if len(s.Production) == 0 {
		s.Production = []string{}
	}
	if len(s.Genres) == 0 {
		s.Genres = []string{}
	}

	return &front.VodBasedStreamFront{
		VODType:       s.VODType,
		TrailerURL:    s.TrailerURL,
		BackgroundURL: s.BackgroundURL,
		UserScore:     s.UserScore,
		PrimeDate:     gofastogt.Time2UtcTimeMsec(s.PrimeDate),
		Country:       s.Country,
		Genres:        s.Genres,
		Production:    s.Production,
		Directors:     s.Directors,
		Cast:          s.Cast,
		Duration:      s.Duration,
	}
}

type VodProxyStream struct {
	ProxyStream    `bson:",inline"`
	VodBasedStream `bson:",inline"`
}

func NewVodProxyStream() *VodProxyStream {
	return &VodProxyStream{
		ProxyStream:    *newProxyStream(media.STREAM_TYPE_VOD_PROXY),
		VodBasedStream: *NewVodBasedStream(),
	}
}

func (s *VodProxyStream) ToFront() *front.VodProxyStreamFront {
	return &front.VodProxyStreamFront{
		ProxyStreamFront:    *s.ProxyStream.ToFront(),
		VodBasedStreamFront: *s.VodBasedStream.ToFront(),
	}
}

func (s *VodProxyStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *VodProxyStream) GetConfig() *media.ProxyVodConfig {
	return &media.ProxyVodConfig{
		ProxyConfig: *s.ProxyStream.GetConfig(),
	}
}

type VodRelayStream struct {
	RelayStream    `bson:",inline"`
	VodBasedStream `bson:",inline"`
}

func NewVodRelayStream() *VodRelayStream {
	return &VodRelayStream{
		RelayStream:    *newRelayStream(media.STREAM_TYPE_VOD_RELAY),
		VodBasedStream: *NewVodBasedStream(),
	}
}

func (s *VodRelayStream) ToFront() *front.VodRelayStreamFront {
	return &front.VodRelayStreamFront{
		RelayStreamFront:    *s.RelayStream.ToFront(),
		VodBasedStreamFront: *s.VodBasedStream.ToFront(),
	}
}

func (s *VodRelayStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *VodRelayStream) GetConfig() *media.VodRelayConfig {
	return &media.VodRelayConfig{RelayConfig: *s.RelayStream.GetConfig()}
}

type VodEncodeStream struct {
	EncodeStream   `bson:",inline"`
	VodBasedStream `bson:",inline"`
}

func NewVodEncodeStream() *VodEncodeStream {
	return &VodEncodeStream{
		EncodeStream:   *newEncodeStream(media.STREAM_TYPE_VOD_ENCODE),
		VodBasedStream: *NewVodBasedStream(),
	}
}

func (s *VodEncodeStream) ToFront() *front.VodEncodeStreamFront {
	return &front.VodEncodeStreamFront{
		EncodeStreamFront:   *s.EncodeStream.ToFront(),
		VodBasedStreamFront: *s.VodBasedStream.ToFront(),
	}
}

func (s *VodEncodeStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *VodEncodeStream) GetConfig() *media.VodEncodeConfig {
	return &media.VodEncodeConfig{
		EncodeConfig: *s.EncodeStream.GetConfig(),
	}
}

type ChangerRelayStream struct {
	RelayStream `bson:",inline"`
}

func NewChangerRelayStream() *ChangerRelayStream {
	return &ChangerRelayStream{
		RelayStream: *newRelayStream(media.STREAM_TYPE_CHANGER_RELAY),
	}
}

func (s *ChangerRelayStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *ChangerRelayStream) ToFront() *front.ChangerRelayStreamFront {
	return &front.ChangerRelayStreamFront{
		RelayStreamFront: *s.RelayStream.ToFront(),
	}
}

func (s *ChangerRelayStream) GetConfig() *media.ChangerRelayConfig {
	return &media.ChangerRelayConfig{
		RelayConfig: *s.RelayStream.GetConfig(),
	}
}

type ChangerEncodeStream struct {
	EncodeStream `bson:",inline"`
}

func NewChangerEncodeStream() *ChangerEncodeStream {
	return &ChangerEncodeStream{
		EncodeStream: *newEncodeStream(media.STREAM_TYPE_CHANGER_ENCODE),
	}
}

func (s *ChangerEncodeStream) ToBytes() (json.RawMessage, error) {
	return json.Marshal(s)
}

func (s *ChangerEncodeStream) ToFront() *front.ChangerEncodeStreamFront {
	return &front.ChangerEncodeStreamFront{
		EncodeStreamFront: *s.EncodeStream.ToFront(),
	}
}

func (s *ChangerEncodeStream) GetConfig() *media.ChangerEncodeConfig {
	return &media.ChangerEncodeConfig{
		EncodeConfig: *s.EncodeStream.GetConfig(),
	}
}

func MakeIStreamToMongo(s *front.IStreamFront) (*IStream, error) {
	var stream IStream
	if s.ID != nil {
		id, err := primitive.ObjectIDFromHex(*s.ID)
		if err != nil {
			return nil, errors.New("invalid id field")
		}
		stream.ID = id
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	} else {
		created = time.Now()
	}

	stream.Name = s.Name
	stream.Price = s.Price
	stream.Visible = s.Visible
	stream.Archive = s.Archive
	stream.IARC = s.IARC
	stream.ViewCount = s.ViewCount
	stream.Output = s.Output
	stream.TypeStream = s.Type
	stream.Subtitles = s.Subtitles
	//optional fields
	stream.TVGName = s.TVGName
	stream.TVGID = s.TVGID
	stream.TVGLogo = s.TVGLogo
	stream.Groups = s.Groups
	stream.Parts = s.Parts
	stream.Meta = s.Meta
	stream.Description = s.Description
	stream.CreatedDate = &created
	stream.Location = s.Location
	return &stream, nil
}

func MakeProxyStreamFromFront(s *front.ProxyStreamFront) (*ProxyStream, error) {
	var stream ProxyStream
	istream, err := MakeIStreamToMongo(&s.IStreamFront)
	if err != nil {
		return nil, err
	}
	stream.IStream = *istream
	return &stream, nil
}

func MakeHardwareStreamFromFront(s *front.HardwareStreamFront) (*HardwareStream, error) {
	var stream HardwareStream
	istream, err := MakeIStreamToMongo(&s.IStreamFront)
	if err != nil {
		return nil, err
	}
	stream.IStream = *istream
	stream.LogLevel = s.LogLevel
	stream.RestartAttempts = s.RestartAttempts
	stream.HaveAudio = s.HaveAudio
	stream.HaveVideo = s.HaveVideo
	stream.Loop = s.Loop
	stream.SelectedInput = s.SelectedInput
	stream.Input = s.Input
	stream.AutoStart = s.AutoStart
	stream.AudioTracksCount = s.AudioTracksCount

	//optional fields
	stream.AutoExitTime = s.AutoExitTime
	stream.ExtraConfig = s.ExtraConfig
	stream.AudioSelect = s.AudioSelect
	stream.NotificationStreamContact = s.NotificationStreamContact

	return &stream, nil
}

func MakeRelayStreamFromFront(s *front.RelayStreamFront) (*RelayStream, error) {
	var stream RelayStream
	hardwarestream, err := MakeHardwareStreamFromFront(&s.HardwareStreamFront)
	if err != nil {
		return nil, err
	}
	stream.HardwareStream = *hardwarestream
	//optional fields
	stream.VideoParser = s.VideoParser
	stream.AudioParser = s.AudioParser

	return &stream, nil
}

func MakeEncodeStreamFromFront(s *front.EncodeStreamFront) (*EncodeStream, error) {
	var stream EncodeStream
	hardwarestream, err := MakeHardwareStreamFromFront(&s.HardwareStreamFront)
	if err != nil {
		return nil, err
	}
	stream.HardwareStream = *hardwarestream
	stream.RelayAudio = s.RelayAudio
	stream.RelayVideo = s.RelayVideo
	stream.VideoCodec = s.VideoCodec
	stream.AudioCodec = s.AudioCodec
	//optional Fields
	stream.Deinterlace = s.Deinterlace
	stream.Resample = s.Resample
	stream.Volume = s.Volume
	stream.FrameRate = s.FrameRate
	stream.AudioChannelsCount = s.AudioChannelsCount
	stream.Size = s.Size
	stream.MachineLearning = s.MachineLearning
	stream.VideoBitrate = s.VideoBitrate
	stream.AudioBitrate = s.AudioBitrate
	stream.Logo = s.Logo
	stream.RsvgLogo = s.RsvgLogo
	stream.AspectRatio = s.AspectRatio
	stream.BackgroundEffect = s.BackgroundEffect
	stream.AudioStabilization = s.AudioStabilization
	stream.TextOverlay = s.TextOverlay
	stream.VideoFlip = s.VideoFlip
	stream.StreamOverlay = s.StreamOverlay
	return &stream, nil
}

func MakeTimeshiftRecorderStreamFromFront(s *front.TimeshiftRecorderStreamFront) (*TimeshiftRecorderStream, error) {
	var stream TimeshiftRecorderStream
	relayStream, err := MakeRelayStreamFromFront(&s.RelayStreamFront)
	if err != nil {
		return nil, err
	}

	stream.RelayStream = *relayStream
	stream.TimeshiftChunkDuration = s.TimeshiftChunkDuration
	stream.TimeshiftChunkLifeTime = s.TimeshiftChunkLifeTime
	return &stream, nil
}

func MakeCatchupStreamFromFront(s *front.CatchupStreamFront) (*CatchupStream, error) {
	var stream CatchupStream
	streamTimeshift, err := MakeTimeshiftRecorderStreamFromFront(&s.TimeshiftRecorderStreamFront)
	if err != nil {
		return nil, err
	}

	stream.TypeStream = media.STREAM_TYPE_CATCHUP
	stream.TimeshiftRecorderStream = *streamTimeshift
	stream.StartRecord = gofastogt.UtcTime2Time(s.StartRecord)
	stream.StopRecord = gofastogt.UtcTime2Time(s.StopRecord)
	return &stream, nil
}

// VODs
func MakeVodBasedStreamFromFront(s *front.VodBasedStreamFront) *VodBasedStream {
	var stream VodBasedStream
	stream.VODType = s.VODType
	stream.TrailerURL = s.TrailerURL
	stream.BackgroundURL = s.BackgroundURL
	stream.UserScore = s.UserScore
	stream.PrimeDate = gofastogt.UtcTime2Time(s.PrimeDate)
	stream.Country = s.Country
	stream.Genres = s.Genres
	stream.Directors = s.Directors
	stream.Production = s.Production
	stream.Cast = s.Cast
	stream.Duration = s.Duration
	return &stream
}

func MakeVodProxyStreamFromFront(s *front.VodProxyStreamFront) (*VodProxyStream, error) {
	var stream VodProxyStream
	proxystream, err := MakeProxyStreamFromFront(&s.ProxyStreamFront)
	if err != nil {
		return nil, err
	}
	stream.ProxyStream = *proxystream
	stream.VodBasedStream = *MakeVodBasedStreamFromFront(&s.VodBasedStreamFront)
	return &stream, nil
}

func MakeVodRelayStreamFromFront(s *front.VodRelayStreamFront) (*VodRelayStream, error) {
	var stream VodRelayStream
	relaystream, err := MakeRelayStreamFromFront(&s.RelayStreamFront)
	if err != nil {
		return nil, err
	}
	stream.RelayStream = *relaystream
	stream.VodBasedStream = *MakeVodBasedStreamFromFront(&s.VodBasedStreamFront)
	return &stream, nil
}

func MakeVodEncodeStreamFromFront(s *front.VodEncodeStreamFront) (*VodEncodeStream, error) {
	var stream VodEncodeStream
	encodeStream, err := MakeEncodeStreamFromFront(&s.EncodeStreamFront)
	if err != nil {
		return nil, err
	}
	stream.EncodeStream = *encodeStream
	stream.VodBasedStream = *MakeVodBasedStreamFromFront(&s.VodBasedStreamFront)
	return &stream, nil
}

// CODs
func MakeCodEncodeStreamFromFront(s *front.CodEncodeStreamFront) (*CodEncodeStream, error) {
	var stream CodEncodeStream
	encodeStream, err := MakeEncodeStreamFromFront(&s.EncodeStreamFront)
	if err != nil {
		return nil, err
	}
	stream.EncodeStream = *encodeStream
	return &stream, nil
}

func MakeCodRelayStreamFromFront(s *front.CodRelayStreamFront) (*CodRelayStream, error) {
	var stream CodRelayStream
	relaystream, err := MakeRelayStreamFromFront(&s.RelayStreamFront)
	if err != nil {
		return nil, err
	}
	stream.RelayStream = *relaystream
	return &stream, nil
}

// CHANGER
func MakeChangerEncodeStreamFromFront(s *front.ChangerEncodeStreamFront) (*ChangerEncodeStream, error) {
	var stream ChangerEncodeStream
	encodeStream, err := MakeEncodeStreamFromFront(&s.EncodeStreamFront)
	if err != nil {
		return nil, err
	}
	stream.EncodeStream = *encodeStream
	return &stream, nil
}

func MakeChangerRelayStreamFromFront(s *front.ChangerRelayStreamFront) (*ChangerRelayStream, error) {
	var stream ChangerRelayStream
	relaystream, err := MakeRelayStreamFromFront(&s.RelayStreamFront)
	if err != nil {
		return nil, err
	}
	stream.RelayStream = *relaystream
	return &stream, nil
}
