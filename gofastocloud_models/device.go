package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DeviceLoginInfo struct {
	IP        string    `bson:"ip" json:"ip"`
	Timestamp time.Time `bson:"timestamp" json:"timestamp"`
	Location  string    `bson:"loc" json:"loc"`
	City      string    `bson:"city" json:"city"`
	Country   string    `bson:"country" json:"country"`
}

type Device struct {
	IDDevice       primitive.ObjectID      `bson:"id" json:"id"`
	Name           string                  `bson:"name" json:"name"`
	Status         front.DeviceStatus      `bson:"status" json:"status"`
	ActiveDuration *gofastogt.DurationMsec `bson:"active_duration,omitempty" json:"active_duration,omitempty"`
	Logins         []DeviceLoginInfo       `bson:"logins,omitempty" json:"logins,omitempty"`
	OS             *front.DeviceOS         `bson:"os,omitempty" json:"os,omitempty"`
	Project        *front.DeviceProject    `bson:"project,omitempty" json:"project,omitempty"`
	CPU            *front.CPUBrand         `bson:"cpu_brand,omitempty" json:"cpu_brand,omitempty"`
	LastActiveTime *time.Time              `bson:"last_active,omitempty" json:"last_active,omitempty"`

	CreatedDate *time.Time `bson:"created_date,omitempty" json:"created_date,omitempty"`
}

func NewDevice() *Device {
	current := time.Now()
	return &Device{
		IDDevice:    primitive.NewObjectID(),
		Name:        "Device",
		CreatedDate: &current,
		Status:      front.NOT_ACTIVE_DEVICE,
	}
}

func (device *Device) ToFront() *front.DeviceLoginFront {
	hexed := device.IDDevice.Hex()
	var created, lastActive gofastogt.UtcTimeMsec
	if device.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*device.CreatedDate)
	}

	if device.LastActiveTime != nil {
		lastActive = gofastogt.Time2UtcTimeMsec(*device.LastActiveTime)
	}

	logins := []front.DeviceLoginInfoFront{}
	for _, info := range device.Logins {
		dev := front.DeviceLoginInfoFront{Timestamp: gofastogt.Time2UtcTimeMsec(info.Timestamp),
			IP: info.IP, Location: info.Location, City: info.City, Country: info.Country}
		logins = append(logins, dev)
	}

	return &front.DeviceLoginFront{
		OS:             device.OS,
		Project:        device.Project,
		CPU:            device.CPU,
		ActiveDuration: device.ActiveDuration,
		Logins:         logins,
		LastActiveTime: &lastActive,
		DevicesFront: front.DevicesFront{
			IDDevice:    &hexed,
			Name:        device.Name,
			Status:      device.Status,
			CreatedDate: &created,
		},
	}
}

func MakeDeviceFromFront(s *front.DevicesFront) *Device {
	var did primitive.ObjectID
	if s.IDDevice != nil {
		did, _ = primitive.ObjectIDFromHex(*s.IDDevice)
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	return &Device{
		Name:        s.Name,
		CreatedDate: &created,
		IDDevice:    did,
		Status:      s.Status,
	}
}
