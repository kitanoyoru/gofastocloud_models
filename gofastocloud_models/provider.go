package gofastocloud_models

import (
	"net/mail"
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ProviderFields struct {
	Email     string               `bson:"email"          json:"email"`
	FirstName string               `bson:"first_name"     json:"first_name"`
	LastName  string               `bson:"last_name"      json:"last_name"`
	Password  string               `bson:"password"       json:"password"`
	Type      front.ProviderType   `bson:"type"           json:"type"`
	Status    front.ProviderStatus `bson:"status"         json:"status"`
	Country   string               `bson:"country"        json:"country"`
	Language  string               `bson:"language"       json:"language"`
	Credits   uint64               `bson:"credits"        json:"credits"`

	// filled by server
	CreatedDate *time.Time           `bson:"created_date,omitempty" json:"created_date,omitempty"`
	Subscribers []primitive.ObjectID `bson:"subscribers"            json:"subscribers"`
}

type Provider struct {
	ID             primitive.ObjectID `bson:"_id" json:"id"`
	ProviderFields `bson:",inline"`
}

func (p *ProviderFields) IsAdmin() bool {
	return p.Type == front.ADMIN
}

func (p *ProviderFields) ContainsSubscriber(sid primitive.ObjectID) bool {
	for _, sub := range p.Subscribers {
		if sub == sid {
			return true
		}
	}

	return false
}

func (p *ProviderFields) IsValidEmail() bool {
	_, err := mail.ParseAddress(p.Email)
	return err == nil
}

func (p *Provider) ToFront(creditsRemaining uint64) *front.ProviderFront {
	hexed := p.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if p.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*p.CreatedDate)
	}

	return &front.ProviderFront{ID: &hexed,
		Email:            p.Email,
		FirstName:        p.FirstName,
		LastName:         p.LastName,
		Password:         &p.Password,
		CreatedDate:      &created,
		Type:             p.Type,
		Status:           p.Status,
		Country:          p.Country,
		Language:         p.Language,
		Credits:          p.Credits,
		CreditsRemaining: creditsRemaining,
	}
}

func MakeProviderFromFront(p *front.ProviderFront) *ProviderFields {
	var created time.Time
	if p.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*p.CreatedDate)
	}

	return &ProviderFields{
		Email:       p.Email,
		FirstName:   p.FirstName,
		LastName:    p.LastName,
		Language:    p.Language,
		Country:     p.Country,
		Password:    *p.Password,
		Type:        p.Type,
		Status:      p.Status,
		Credits:     p.Credits,
		CreatedDate: &created,
	}
}
