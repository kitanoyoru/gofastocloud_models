package gofastocloud_models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SubscriptionFields struct {
	PackageID primitive.ObjectID `bson:"package"             json:"package"`
	Payment   string             `bson:"payment"             json:"payment"`
}

type Subscription struct {
	ID                 primitive.ObjectID `bson:"_id" json:"id"`
	SubscriptionFields `bson:",inline"`
}

func NewSubscription(id *primitive.ObjectID, pid string) *SubscriptionFields {
	return &SubscriptionFields{
		PackageID: *id,
		Payment:   pid,
	}
}
