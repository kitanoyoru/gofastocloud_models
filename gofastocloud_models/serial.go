package gofastocloud_models

import (
	"encoding/json"
	"errors"
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SerialField struct {
	Name          string               `bson:"name"           json:"name"`
	BackgroundURL string               `bson:"background_url" json:"background_url"`
	Icon          string               `bson:"icon"           json:"icon"`
	IARC          int                  `bson:"iarc" json:"iarc"`
	Groups        []string             `bson:"groups" json:"groups"`
	Description   string               `bson:"description"    json:"description"`
	PrimeDate     time.Time            `bson:"prime_date" json:"prime_date"`
	Seasons       []primitive.ObjectID `bson:"seasons"        json:"seasons"`
	Price         float64              `bson:"price" json:"price"`
	Visible       bool                 `bson:"visible" json:"visible"`
	ViewCount     int                  `bson:"view_count" json:"view_count"`
	UserScore     float64              `bson:"user_score" json:"user_score"`
	Country       string               `bson:"country" json:"country"`
	Genres        []string             `bson:"genres" json:"genres"`
	Directors     []string             `bson:"directors" json:"directors"`
	Cast          []string             `bson:"cast" json:"cast"`
	Production    []string             `bson:"production" json:"production"`

	// server setup
	CreatedDate *time.Time `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

type Serial struct {
	ID          primitive.ObjectID `bson:"_id" json:"id"`
	SerialField `bson:",inline"`
}

func (s *Serial) ToFront() *front.SerialFront {
	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}

	seasons := []string{}
	for _, season := range s.Seasons {
		seasons = append(seasons, season.Hex())
	}
	if len(s.Groups) == 0 {
		s.Groups = []string{}
	}
	if len(s.Cast) == 0 {
		s.Cast = []string{}
	}
	if len(s.Production) == 0 {
		s.Production = []string{}
	}
	if len(s.Genres) == 0 {
		s.Genres = []string{}
	}
	if len(s.Directors) == 0 {
		s.Directors = []string{}
	}
	return &front.SerialFront{
		ID:          &hexed,
		Name:        s.Name,
		Background:  s.BackgroundURL,
		Icon:        s.Icon,
		IARC:        s.IARC,
		PrimeDate:   gofastogt.Time2UtcTimeMsec(s.PrimeDate),
		Description: s.Description,
		Seasons:     seasons,
		Price:       s.Price,
		Visible:     s.Visible,
		ViewCount:   s.ViewCount,
		Groups:      s.Groups,
		CreatedDate: &created,
		UserScore:   s.UserScore,
		Country:     s.Country,
		Cast:        s.Cast,
		Genres:      s.Genres,
		Production:  s.Production,
		Directors:   s.Directors,
	}
}

func MakeSerialFromFront(s *front.SerialFront) (*Serial, error) {
	var serial Serial
	if s.ID != nil {
		id, err := primitive.ObjectIDFromHex(*s.ID)
		if err != nil {
			return nil, errors.New("invalid id field")
		}
		serial.ID = id
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
		serial.CreatedDate = &created
	}
	var seasons []primitive.ObjectID

	for _, season := range s.Seasons {
		id, err := primitive.ObjectIDFromHex(season)
		if err != nil {
			return nil, err
		}
		seasons = append(seasons, id)
	}
	serial.Seasons = seasons
	serial.Name = s.Name
	serial.Icon = s.Icon
	serial.IARC = s.IARC
	serial.Description = s.Description
	serial.Groups = s.Groups
	serial.PrimeDate = gofastogt.UtcTime2Time(s.PrimeDate)
	serial.BackgroundURL = s.Background
	serial.Price = s.Price
	serial.ViewCount = s.ViewCount
	serial.Visible = s.Visible
	serial.UserScore = s.UserScore
	serial.Country = s.Country
	serial.Cast = s.Cast
	serial.Directors = s.Directors
	serial.Production = s.Production
	serial.Genres = s.Genres
	return &serial, nil
}

func (s *Serial) ToByte() (json.RawMessage, error) {
	return json.Marshal(s)
}
