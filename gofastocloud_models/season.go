package gofastocloud_models

import (
	"encoding/json"
	"errors"
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type SeasonField struct {
	Name          string               `bson:"name"           json:"name"`
	Icon          string               `bson:"icon"           json:"icon"`
	BackgroundURL string               `bson:"background_url" json:"background_url"`
	Groups        []string             `bson:"groups"         json:"groups"`
	Description   string               `bson:"description"    json:"description"`
	Season        int                  `bson:"season"         json:"season"`
	Episodes      []primitive.ObjectID `bson:"episodes"       json:"episodes"`
	ViewCount     int                  `bson:"view_count"     json:"view_count"`

	// server setup
	CreatedDate *time.Time `bson:"created_date,omitempty"         json:"created_date,omitempty"`
}

type Season struct {
	ID          primitive.ObjectID `bson:"_id" json:"id"`
	SeasonField `bson:",inline"`
}

func NewSeasonField() *SeasonField {
	current := time.Now()
	return &SeasonField{
		CreatedDate: &current,
	}
}

func (s *Season) ToFront() *front.SeasonFront {
	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}

	episodes := []string{}
	for _, episode := range s.Episodes {
		episodes = append(episodes, episode.Hex())
	}
	if len(s.Groups) == 0 {
		s.Groups = []string{}
	}
	return &front.SeasonFront{
		ID:          &hexed,
		Name:        s.Name,
		Background:  s.BackgroundURL,
		Icon:        s.Icon,
		Groups:      s.Groups,
		Description: s.Description,
		Season:      s.Season,
		ViewCount:   s.ViewCount,
		Episodes:    episodes,
		CreatedDate: &created,
	}
}

func MakeSeasonFromFront(s *front.SeasonFront) (*Season, error) {
	var serial Season
	if s.ID != nil {
		id, err := primitive.ObjectIDFromHex(*s.ID)
		if err != nil {
			return nil, errors.New("invalid id field")
		}
		serial.ID = id
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
		serial.CreatedDate = &created
	}
	var episodes []primitive.ObjectID

	for _, episode := range s.Episodes {
		e, err := primitive.ObjectIDFromHex(episode)
		if err != nil {
			return nil, err
		}
		episodes = append(episodes, e)
	}
	serial.Episodes = episodes
	serial.Name = s.Name
	serial.Icon = s.Icon
	serial.Groups = s.Groups
	serial.Description = s.Description
	serial.Season = s.Season
	serial.BackgroundURL = s.Background
	serial.ViewCount = s.ViewCount

	return &serial, nil
}

func (s *Season) ToByte() (json.RawMessage, error) {
	return json.Marshal(s)
}
