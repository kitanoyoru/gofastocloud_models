package gofastocloud_models

import (
	"encoding/json"
	"errors"
	"net/url"
	"strings"

	"gitlab.com/fastogt/gofastocloud_base/gofastocloud_base"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type IPAddress struct {
	IP string `yaml:"ip" json:"ip"`
}

type NodeSettings struct {
	Host string                       `yaml:"host" json:"host"`
	Key  gofastocloud_base.LicenseKey `yaml:"key" json:"key"`
}

type AuthContent int

const (
	JWT_CONTENT_AUTH AuthContent = iota
	BASE_CONTENT_AUTH
)

type WebRTCSettings struct {
	Stun string `yaml:"stun" json:"stun"`
	Turn string `yaml:"turn" json:"turn"`
}

type HttpsConfig struct {
	Key  string `yaml:"key" json:"key"`
	Cert string `yaml:"cert" json:"cert"`
}

type GoBackendSettings struct {
	Host               string       `yaml:"host" json:"host"`
	Alias              string       `yaml:"alias" json:"alias"`
	HlsHost            string       `yaml:"hls_host" json:"hls_host"`
	VodsHost           string       `yaml:"vods_host" json:"vods_host"`
	CodsHost           string       `yaml:"cods_host" json:"cods_host"`
	PreferredUrlScheme string       `yaml:"preferred_url_scheme" json:"preferred_url_scheme"`
	HttpsConfig        *HttpsConfig `yaml:"https,omitempty" json:"https,omitempty"`

	BlackList []IPAddress `yaml:"blacklist" json:"blacklist"`

	Node *NodeSettings `yaml:"node,omitempty" json:"node,omitempty"`

	LogLevel          string           `yaml:"log_level" json:"log_level"`
	LogPath           string           `yaml:"log_path" json:"log_path"`
	Cors              bool             `yaml:"cors" json:"cors"`
	Auth              *string          `yaml:"auth,omitempty" json:"auth,omitempty"`
	AuthContent       AuthContent      `yaml:"auth_content" json:"auth_content"`
	VodInfo           *VodInfoSettings `yaml:"vod_info,omitempty" json:"vod_info,omitempty"`
	AlarmNotification *Notification    `yaml:"alarm,omitempty" json:"alarm,omitempty"`

	MaxUploadFileSize int64           `yaml:"max_upload_file_size" json:"max_upload_file_size"`
	CatcupTTL         int             `yaml:"catchup_ttl" json:"catchup_ttl"`
	WebRTC            *WebRTCSettings `yaml:"webrtc,omitempty" json:"webrtc,omitempty"`

	// internal fields
	Master string `yaml:"master" json:"-"`
}

func GetTrueHost(url url.URL, alias *string) url.URL {
	if alias != nil {
		port := url.Port()
		url.Host = *alias
		if len(port) > 0 {
			url.Host += ":" + port
		}
	}

	return url
}

func (settings *GoBackendSettings) GetHlsHost() url.URL {
	hls, _ := url.Parse(settings.HlsHost)
	return *hls
}

func (settings *GoBackendSettings) GetVodsHost() url.URL {
	hls, _ := url.Parse(settings.VodsHost)
	return *hls
}

func (settings *GoBackendSettings) GetCodsHost() url.URL {
	hls, _ := url.Parse(settings.CodsHost)
	return *hls
}

func (settings *GoBackendSettings) GetHlsUrl() url.URL {
	hls, _ := url.Parse(settings.HlsHost)
	return GetTrueHost(*hls, &settings.Alias)
}

func (settings *GoBackendSettings) GetVodsUrl() url.URL {
	vods, _ := url.Parse(settings.VodsHost)
	return GetTrueHost(*vods, &settings.Alias)
}

func (settings *GoBackendSettings) GetCodsUrl() url.URL {
	cods, _ := url.Parse(settings.CodsHost)
	return GetTrueHost(*cods, &settings.Alias)
}

type VodInfoSettings struct {
	Type TypeVodService `json:"type" yaml:"type"`

	Tmdb *TmdbSettings `json:"tmdb,omitempty" yaml:"tmdb,omitempty"`
}

type TypeVodService int

const (
	DUMMY TypeVodService = iota
	TMDB
)

type TmdbSettings struct {
	Key string `yaml:"key" json:"key"`
}

type Notification struct {
	Type         NotificationType     `json:"type" yaml:"type"`
	EmailService *EmailServerSettings `json:"email_server_settings,omitempty" yaml:"email_server_settings,omitempty"`
}

func (n *Notification) UnmarshalJSON(data []byte) error {
	request := struct {
		Type         *NotificationType    `json:"type"`
		EmailService *EmailServerSettings `json:"email_server_settings,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Type == nil {
		return errors.New("type field is required")
	}
	if request.Type != nil {
		if !request.Type.IsValid() {
			return errors.New("invalid input Notification Type")
		}
		if *request.Type == EMAIL_NOTIFICATION && request.EmailService != nil {
			n.Type = *request.Type
			n.EmailService = request.EmailService
		} else {
			return errors.New("invalid input Notification")
		}
	}
	return nil
}

type NotificationType int

const (
	EMAIL_NOTIFICATION NotificationType = iota
)

func (n NotificationType) IsValid() bool {
	return n == EMAIL_NOTIFICATION
}

type EmailServerSettings struct {
	MailServer   string `json:"mail_server" yaml:"mail_server"`
	MailUser     string `json:"user" yaml:"user"`
	MailPassword string `json:"mail_password" yaml:"mail_password"`
	MailPort     int    `json:"mail_port" yaml:"mail_port"`
}

func CreateDefaultSettings() *GoBackendSettings {

	var s = &GoBackendSettings{
		Host:               "0.0.0.0:8088",
		Alias:              "0.0.0.0",
		HlsHost:            "http://0.0.0.0:8010",
		VodsHost:           "http://0.0.0.0:7010",
		CodsHost:           "http://0.0.0.0:6010",
		PreferredUrlScheme: "http",
		LogPath:            "~/gofastocloud.log",
		LogLevel:           "INFO",
		Cors:               false,
		MaxUploadFileSize:  10,

		Master:      "master:master",
		AuthContent: BASE_CONTENT_AUTH,
	}

	return s
}

func (c AuthContent) IsValid() bool {
	switch c {
	case JWT_CONTENT_AUTH, BASE_CONTENT_AUTH:
		return true
	}
	return false
}

func (node *NodeSettings) GetHost() *gofastogt.HostAndPort {
	host, err := gofastogt.MakeHostAndPortFromString(node.Host)
	if err != nil {
		return nil
	}

	return host
}

func (n *NodeSettings) UnmarshalJSON(data []byte) error {
	request := struct {
		Host string                       `json:"host"`
		Key  gofastocloud_base.LicenseKey `json:"key"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if len(request.Host) == 0 {
		return errors.New("host field is required and can't be empty")
	}
	if len(request.Key) == 0 {
		return errors.New("key field is required and can't be empty")
	}
	if !request.Key.IsValid() {
		return errors.New("key field is not valid")
	}
	n.Host = request.Host
	n.Key = request.Key
	return nil
}

func (https *HttpsConfig) UnmarshalJSON(data []byte) error {
	request := struct {
		Key  string `json:"key"`
		Cert string `json:"cert"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if len(request.Key) == 0 {
		return errors.New("httpsConfig->key field is required and can't be empty")
	}
	if len(request.Cert) == 0 {
		return errors.New("httpsConfig->cert field is required and can't be empty")
	}
	https.Key = request.Key
	https.Cert = request.Cert
	return nil
}

func (s *GoBackendSettings) UnmarshalJSON(data []byte) error {
	request := struct {
		Host               string        `json:"host"`
		HlsHost            string        `json:"hls_host"`
		VodsHost           string        `json:"vods_host"`
		CodsHost           string        `json:"cods_host"`
		Alias              string        `json:"alias"`
		Node               *NodeSettings `json:"node"`
		PreferredUrlScheme string        `json:"preferred_url_scheme"`
		HttpsConfig        *HttpsConfig  `json:"https,omitempty"`
		BlackList          []IPAddress   `json:"blacklist"`

		LogLevel          string           `json:"log_level"`
		LogPath           string           `json:"log_path"`
		Cors              *bool            `json:"cors"`
		Auth              string           `json:"auth,omitempty"`
		AuthContent       *int             `json:"auth_content"`
		VodInfo           *VodInfoSettings `json:"vod_info,omitempty"`
		AlarmNotification *Notification    `json:"alarm,omitempty"`

		MaxUploadFileSize *int64          `json:"max_upload_file_size"`
		WebRTC            *WebRTCSettings `json:"webrtc,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if len(request.Host) == 0 {
		return errors.New("host field is required and can't be empty")
	}
	if _, err := gofastogt.MakeHostAndPortFromString(request.Host); err != nil {
		return errors.New("host field should have format host:port")
	}

	if len(request.LogLevel) == 0 {
		return errors.New("logLevel field is required and can't be empty")
	}
	if len(request.PreferredUrlScheme) == 0 {
		return errors.New("preferredUrlScheme field is required and can't be empty")
	}
	if len(request.LogPath) == 0 {
		return errors.New("logPath field is required and can't be empty")
	}
	if request.Cors == nil {
		return errors.New("cors field is required")
	}
	if request.MaxUploadFileSize == nil {
		return errors.New("max_upload_file_size field is required")
	}
	if len(request.Auth) != 0 {
		auth := strings.Split(request.Auth, ":")
		if len(auth) != 2 {
			return errors.New("auth field should have format username:password")
		}
		s.Auth = &request.Auth
	}
	if request.WebRTC != nil {
		s.WebRTC = request.WebRTC
	}
	if request.HttpsConfig != nil {
		s.HttpsConfig = request.HttpsConfig
	}
	if request.Node != nil {
		s.Node = request.Node
	}
	if request.AuthContent == nil {
		return errors.New("auth_content filed is required")
	}
	if request.VodInfo != nil {
		s.VodInfo = request.VodInfo
	}
	if request.AlarmNotification != nil {
		s.AlarmNotification = request.AlarmNotification
	}

	authContent := AuthContent(*request.AuthContent)
	if !authContent.IsValid() {
		return errors.New("invalid data auth_content filed")
	}

	hls, err := url.Parse(request.HlsHost)
	if err != nil {
		return errors.New("invalid hls url")
	}

	vods, err := url.Parse(request.VodsHost)
	if err != nil {
		return errors.New("invalid vods url")
	}

	cods, err := url.Parse(request.CodsHost)
	if err != nil {
		return errors.New("invalid cods url")
	}

	s.AuthContent = authContent
	s.Host = request.Host
	s.HlsHost = hls.String()
	s.VodsHost = vods.String()
	s.CodsHost = cods.String()
	s.LogLevel = request.LogLevel
	s.LogPath = request.LogPath
	s.Cors = *request.Cors
	s.MaxUploadFileSize = *request.MaxUploadFileSize
	s.PreferredUrlScheme = request.PreferredUrlScheme
	s.BlackList = request.BlackList
	s.Alias = request.Alias

	return nil
}
